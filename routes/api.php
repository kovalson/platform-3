<?php

use Illuminate\Support\Facades\Route;

// We use the API namespace group not to repeat the namespace in all routes
Route::group(["namespace" => "Api"], function () {

    // Status routes
    Route::get("/status/{registrationNumber}", "StatusController@index");

    // We restrict some routes for guests only
    Route::middleware("guest")->group(function () {
        Route::post("/login", "AuthController@login");
    });

    // We restrict some routes for authorized users only
    Route::middleware("auth:api")->group(function () {

        // Basic resources routes
        Route::resources([
            "roles" => "RoleController",
            "districts" => "DistrictController",
            "units" => "UnitController",
            "users" => "UserController",
            "scouts" => "ScoutController"
        ]);

        // Contributions routes
        Route::patch("/contributions/update", "ContributionController@update");

        // Contributions amounts routes
        Route::get("/contributions/amounts", "ContributionAmountController@index");
        Route::patch("/contributions/amounts", "ContributionAmountController@update");

        // Settings routes
        Route::get("/settings", "SettingController@index");
        Route::patch("/settings", "SettingController@update");

        // Account routes
        Route::patch("/account", "AccountController@update");

    });

});
