<?php

namespace {
    exit("This file should not be included, only analyzed be the IDE.");
}

namespace Illuminate\Contracts\Routing {

    use Illuminate\Contracts\Pagination\LengthAwarePaginator;
    use Illuminate\Http\JsonResponse;

    interface ResponseFactory
    {
        /**
         * Returns a JSON response with an array that contains data items from the
         * paginator under given data key and pagination data under "pagination" key.
         *
         * @param  LengthAwarePaginator  $paginator
         * @param  string  $dataKey
         * @return JsonResponse
         */
        public function paginated(LengthAwarePaginator $paginator, string $dataKey);
    }

}

namespace Illuminate\Contracts\Auth {

    use App\Models\User;

    interface Guard
    {
        /**
         * Returns currently authenticated user.
         *
         * @return User
         */
        public function user(): User;
    }
}
