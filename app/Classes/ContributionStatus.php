<?php
declare(strict_types=1);
namespace App\Classes;

/**
 * Class ContributionStatus
 *
 * @package App\Classes
 */
class ContributionStatus
{
    /**
     * The contribution is fully paid.
     *
     * @type int
     */
    const PAID = 1;

    /**
     * The contribution is not paid.
     *
     * @type int
     */
    const UNPAID = 2;

    /**
     * The contribution is out of range for current year/quarter.
     *
     * @type int
     */
    const OUT_OF_RANGE = 3;

    /**
     * The check for status is not applicable to the contribution.
     *
     * @type int
     */
    const NOT_APPLICABLE = 4;
}
