<?php
declare(strict_types=1);
namespace App\Classes;

use App\Models\ContributionAmount;
use App\Models\Scout;
use App\Models\Setting;
use Carbon\CarbonInterface;
use Illuminate\Support\Carbon;

/**
 * Class ContributionsProfile
 *
 * @package App\Classes
 */
class ContributionsProfile
{
    /**
     * The scout model.
     *
     * @var Scout
     */
    private $scout;

    /**
     * The starting year of contributions calculations (inclusive).
     *
     * @var int
     */
    private $yearStart;

    /**
     * The ending year of contributions calculations (inclusive).

     * @var int
     */
    private $yearEnd;

    /**
     * The last day of the month to charge for.
     *
     * @var int
     */
    private $lastChargeDay;

    /**
     * The array of current contributions amounts values.
     *
     * @var array
     */
    private $contributionsAmounts;

    /**
     * The current date.
     *
     * @var CarbonInterface
     */
    private $currentDate;

    /**
     * The resulting contributions profile array.
     *
     * @var array
     */
    private $profile;

    /**
     * ContributionsProfile constructor.
     *
     * @param  Scout  $scout
     * @return void
     */
    public function __construct(Scout $scout)
    {
        $settings = Setting::getSettingsArray();

        $this->scout                = $scout;
        $this->yearStart            = (int) $settings[Setting::CONTRIBUTIONS_YEAR_START];
        $this->yearEnd              = (int) $settings[Setting::CONTRIBUTIONS_YEAR_END];
        $this->lastChargeDay        = (int) $settings[Setting::CONTRIBUTIONS_LAST_CHARGE_DAY];
        $this->contributionsAmounts = ContributionAmount::getContributionsAmountsArray();
        $this->currentDate          = Carbon::today();
        $this->profile              = [];

        $this->createContributionsProfile();
    }

    /**
     * Creates the contributions profile array.
     *
     * @return void
     */
    private function createContributionsProfile(): void
    {
        // We initialize the profile array
        $this->profile = [
            "scout_id" => $this->scout->id,
            "years" => [],
            "yearsCount" => 0,
            "status" => [],
            "arrears" => [
                "total" => 0,
                "array" => [],
                "quartersCount" => 0
            ]
        ];

        // We loop through all years range
        for ($year = $this->yearStart; $year <= $this->yearEnd; $year++) {

            // We register the year in the profile
            $this->profile["years"][] = $year;
            $this->profile["yearsCount"]++;

            // We extend the profile status array
            $this->profile["status"][$year] = [];

            // We loop through all quarters of year
            for ($quarter = 1; $quarter <= 4; $quarter++) {
                $status = $this->getStatus($year, $quarter);

                // We save the status in the profile array
                $this->profile["status"][$year]["quarter" . $quarter] = $status;

                if ($status === ContributionStatus::UNPAID) {
                    $arrears = $this->getArrearsFor($year, $quarter);

                    if (!isset($this->profile["arrears"]["array"][$year])) {
                        $this->profile["arrears"]["array"][$year] = [];
                    }

                    $this->profile["arrears"]["array"][$year]["quarter" . $quarter] = $arrears;
                    $this->profile["arrears"]["total"] += $arrears;
                    $this->profile["arrears"]["quartersCount"]++;
                }
            }
        }
    }

    /**
     * Returns the arrears for given year and quarter.
     *
     * @param  int  $year
     * @param  int  $quarter
     * @return int
     */
    private function getArrearsFor(int $year, int $quarter): int
    {
        $hasYear = isset($this->contributionsAmounts[$year]);
        $hasQuarter = isset($this->contributionsAmounts[$year]["quarter" . $quarter]);

        return ($hasYear && $hasQuarter)
            ? $this->contributionsAmounts[$year]["quarter" . $quarter]
            : 0;
    }

    /**
     * Returns the contribution status for given year and quarter.
     *
     * @param  int  $year
     * @param  int  $quarter
     * @return int
     */
    private function getStatus(int $year, int $quarter): int
    {
        // We initialize the status as "unpaid"
        $status = ContributionStatus::UNPAID;

        if ($this->isContributionPaidFor($year, $quarter)) {
            $status = ContributionStatus::PAID;
        }

        if ($status !== ContributionStatus::PAID) {
            if (!$this->isContributionApplicableFor($year, $quarter)) {
                $status = ContributionStatus::NOT_APPLICABLE;
            }

            if ($this->isContributionOutOfRangeFor($year, $quarter)) {
                $status = ContributionStatus::OUT_OF_RANGE;
            }
        }

        return $status;
    }

    /**
     * Checks whether the contributions is fully paid for given year and quarter.
     *
     * @param  int  $year
     * @param  int  $quarter
     * @return bool
     */
    private function isContributionPaidFor(int $year, int $quarter): bool
    {
        return $this->scout->hasPaidContributionFor($year, $quarter);
    }

    /**
     * Checks whether contribution may be applied to scout within his join and quit dates.
     *
     * @param  int  $year
     * @param  int  $quarter
     * @return bool
     */
    private function isContributionApplicableFor(int $year, int $quarter): bool
    {
        $joinedAt = $this->scout->joined_at
            ? Carbon::createFromFormat("Y-m-d", $this->scout->joined_at)
            : null;

        $quitAt = $this->scout->quit_at
            ? Carbon::createFromFormat("Y-m-d", $this->scout->quit_at)
            : null;

        // The contribution is always applicable when the scout
        // has neither of the dates defined
        if (!$joinedAt && !$quitAt) {
            return true;
        }

        // We get the payment deadline
        $deadline = $this->getDeadlineFor($year, $quarter);

        if ($joinedAt && $quitAt) {
            return $deadline->between($joinedAt, $quitAt, true);
        } else if ($joinedAt && $joinedAt->greaterThan($deadline)) {
            return false;
        } else if ($quitAt && $quitAt->lessThan($deadline)) {
            return false;
        }

        return true;
    }

    /**
     * Checks whether the contribution is larger than last charge day.
     *
     * @param  int  $year
     * @param  int  $quarter
     * @return bool
     */
    private function isContributionOutOfRangeFor(int $year, int $quarter): bool
    {
        $deadline = $this->getDeadlineFor($year, $quarter);

        return $this->currentDate->lessThan($deadline);
    }

    /**
     * Returns the payment deadline for given year and quarter.
     *
     * @param  int  $year
     * @param  int  $quarter
     * @return CarbonInterface
     */
    private function getDeadlineFor(int $year, int $quarter): CarbonInterface
    {
        // The [year, month, day] parts of deadline date. The month
        // is calculated as the very first month of given quarter
        $deadlineParts = [$year, 3 * $quarter - 2, $this->lastChargeDay];

        return Carbon::createFromFormat("Y-m-d", join("-", $deadlineParts));
    }

    /**
     * Returns the profile array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->profile;
    }
}
