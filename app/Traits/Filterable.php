<?php
declare(strict_types=1);
namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Filterable
{
    /**
     * Filters the results using given columns array.
     * Also merges that array with columns stored in class property.
     *
     * @see Filterable::scopeFiltered()
     * @param  Builder  $query
     * @param  array  $columns
     * @return Builder
     */
    public function scopeFilteredWithColumns(Builder $query, array $columns): Builder
    {
        // We check if there is a property storing columns to search by
        // and if so we merge these columns with the ones passed as argument
        if (isset($this->filterColumns) && is_array($this->filterColumns)) {
            $columns = array_merge($columns, $this->filterColumns);
        }

        // We return the scoped query
        return $this->scopeFiltered($query, $columns);
    }

    /**
     * Filters the results using values sent in the request.
     *
     * @param  Builder  $query
     * @param  array  $filterColumns
     * @return Builder
     */
    public function scopeFiltered(Builder $query, array $filterColumns = []): Builder
    {
        // First we get filter columns passed as argument and if these are empty
        // we take the ones that come from the class property
        if (empty($filterColumns) && isset($this->filterColumns) && is_array($this->filterColumns)) {
            $filterColumns = $this->filterColumns;
        }

        // If filter columns array is still empty we return the query as there
        // is nothing to do
        if (empty($filterColumns)) {
            return $query;
        }

        // We get the request object
        $request = request();

        // We loop through all filter columns
        foreach ($filterColumns as $column) {

            // We ignore columns which do not appear in the request
            if (!$request->has($column)) {
                continue;
            }

            // We append the filter to the query
            $query->where($column, $request->input($column));

        }

        // We return the updated query
        return $query;
    }
}
