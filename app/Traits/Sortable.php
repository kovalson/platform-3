<?php
declare(strict_types=1);
namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Sortable
{
    /**
     * Sorts the result using given array of columns and directions.
     *
     * @param  Builder  $query
     * @param  array  $sortColumns
     * @return Builder
     */
    public function scopeSorted(Builder $query, array $sortColumns = [])
    {
        // First we get sorting columns passed as argument and if these are empty
        // we take the ones that come from the class property
        if (empty($sortColumns) && isset($this->sortColumns) && is_array($this->sortColumns)) {
            $sortColumns = $this->sortColumns;
        }

        // If sorting columns array is still empty we return the query as there
        // is nothing to do
        if (empty($sortColumns)) {
            return $query;
        }

        // We loop through all sorting columns
        foreach ($sortColumns as $column => $order) {

            // If the key has no value - use ascending order
            if (is_integer($column)) {
                $query->orderBy($order, "asc");
            } else {
                $query->orderBy($column, $order);
            }

        }

        // We return the query
        return $query;
    }
}
