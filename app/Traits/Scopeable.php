<?php
declare(strict_types=1);
namespace App\Traits;

use App\Exceptions\UnauthorizedResourceActionException;
use App\Http\Helpers\RequestKey;
use App\Models\RolePermissionScope;
use Illuminate\Database\Eloquent\Builder;

trait Scopeable
{
    /**
     * Checks whether the target user has access to action related
     * to given permission for this resource.
     *
     * @param  string  $permission
     * @return void
     * @throws UnauthorizedResourceActionException
     */
    public function checkResourceActionAccess(string $permission): void
    {
        // We do not need to check on a super administrator
        if (auth()->user()->isSuperAdministrator()) {
            return;
        }

        // We check if this resource exists in all scoped results
        $exists = self::scoped($permission)
            ->whereId($this->id)
            ->exists();

        if (!$exists) {
            throw new UnauthorizedResourceActionException();
        }
    }

    /**
     * Uses role permission scopes on model for requesting user.
     *
     * @param  Builder  $query
     * @param  string|null  $permission
     * @return Builder
     */
    public function scopeScoped(Builder $query, string $permission = null): Builder
    {
        // We retrieve the permission name for scope
        $permissionName = $permission;

        if (is_null($permissionName)) {
            $permissionName = request()->input(RequestKey::MODEL_PERMISSION) ?? config("permissions.role.access");
        }

        // We retrieve the scope models
        $scopes = RolePermissionScope::findForUserPermission($permissionName);

        // We wrap the whole query to prevent alternations from interfering
        // with other possible queries
        $query->where(function (Builder $query) use ($scopes) {
            // We loop through all scopes
            $scopes->each(function (RolePermissionScope $scope) use (&$query) {
                switch ($scope->scope) {
                    case config("role_permission_scopes.ids"):
                        $query = $this->scopeWithIds($query, $scope->getValuesArray(), $this->getTable());
                        break;
                    case config("role_permission_scopes.roles"):
                        $query = $this->scopeWithRoles($query, $scope->getValuesArray());
                        break;
                    case config("role_permission_scopes.common.roles"):
                        $query = $this->scopeCommonRoles($query);
                        break;
                    case config("role_permission_scopes.common.districts"):
                        $query = $this->scopeCommonDistricts($query);
                        break;
                    case config("role_permission_scopes.common.units"):
                        $query = $this->scopeCommonUnits($query);
                        break;
                    case config("role_permission_scopes.referencing.district"):
                        $query = $this->scopeReferencingDistrict($query, (int) $scope->values);
                        break;
                    case config("role_permission_scopes.referencing.unit"):
                        $query = $this->scopeReferencingUnit($query, (int) $scope->values);
                        break;
                    case config("role_permission_scopes.related.districts"):
                        $query = $this->scopeRelatedToDistricts($query, $scope->getValuesArray());
                        break;
                    case config("role_permission_scopes.related.units"):
                        $query = $this->scopeRelatedToUnits($query, $scope->getValuesArray());
                        break;
                }
            });
        });

        return $query;
    }

    /**
     * Scopes the query to results with given ids.
     *
     * @param  Builder  $query
     * @param  array  $ids
     * @param  string  $table
     * @param  string  $column
     * @return Builder
     */
    public function scopeWithIds(Builder $query, array $ids = [], string $table = "", string $column = "id"): Builder
    {
        // First we check if there are any ids given
        if (empty($ids)) {
            return $query;
        }

        // Because in some queries it could be ambiguous to use only column name
        // we might want to apply the table name as well
        if ($table !== "") {
            $column = $table . "." . $column;
        }

        // We apply the scope
        return $query->orWhereIn($column, $ids);
    }

    /**
     * Scopes the query to results with given roles.
     * This scope may be applied only to models that can have roles.
     *
     * @param  Builder  $query
     * @param  array  $roles
     * @param  string  $column
     * @return Builder
     */
    public function scopeWithRoles(Builder $query, array $roles = [], string $column = "id"): Builder
    {
        // First we check if there are any roles given
        if (empty($roles)) {
            return $query;
        }

        // We go to roles relationship
        return $query->orWhereHas("roles", function (Builder $query) use ($roles, $column) {

            // We loop through all roles
            $query->whereIn($column, $roles);

        });

    }

    /**
     * Scopes the query to results that reference given district id.
     *
     * @param  Builder  $query
     * @param  int|array  $district_id
     * @param  string  $referenceKey
     * @return Builder
     */
    public function scopeReferencingDistrict(Builder $query, $district_id, string $referenceKey = "district_id"): Builder
    {
        return $this->scopeReferencingIntegerKey($query, $referenceKey, $district_id);
    }

    /**
     * Scopes the query to results that reference given unit id.
     *
     * @param  Builder  $query
     * @param  int|array  $unit_id
     * @param  string  $referenceKey
     * @return Builder
     */
    public function scopeReferencingUnit(Builder $query, $unit_id, string $referenceKey = "unit_id"): Builder
    {
        return $this->scopeReferencingIntegerKey($query, $referenceKey, $unit_id);
    }

    /**
     * Scopes the query to results that are related to given districts.
     *
     * @param  Builder  $query
     * @param  array  $districts
     * @param  string  $relationship
     * @return Builder
     */
    public function scopeRelatedToDistricts(Builder $query, array $districts = [], string $relationship = "districts"): Builder
    {
        return $this->scopeRelatedTo($query, $relationship, $districts);
    }

    /**
     * Scopes the query to results that are related to given units.
     *
     * @param  Builder  $query
     * @param  array  $units
     * @param  string  $relationship
     * @return Builder
     */
    public function scopeRelatedToUnits(Builder $query, array $units = [], string $relationship = "units"): Builder
    {
        return $this->scopeRelatedTo($query, $relationship, $units);
    }

    /**
     * Scopes the query to results that share the same roles.
     * This scope may be applied only to models that can have roles.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeCommonRoles(Builder $query): Builder
    {
        return $this->scopeCommonRelationship($query, "roles");
    }

    /**
     * Scopes the query to results that share the same districts.
     * This scope may be applied only to models that can have districts.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeCommonDistricts(Builder $query): Builder
    {
        return $this->scopeCommonRelationship($query, "districts");
    }

    /**
     * Scopes the query to results that share the same units.
     * This scope may be applied only to models that can have units.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeCommonUnits(Builder $query): Builder
    {
        return $this->scopeCommonRelationship($query, "units");
    }

    /**
     * Scopes the query to results with given array of values within given relationship.
     *
     * @param  Builder  $query
     * @param  string  $relationship
     * @param  array  $array
     * @param  string  $column
     * @return Builder
     */
    private function scopeRelatedTo(Builder $query, string $relationship, array $array, string $column = "id"): Builder
    {
        // First we check if given array contains any values
        if (empty($array)) {
            return $query;
        }

        // We apply the scope
        return $query->orWhereHas($relationship, function (Builder $query) use ($relationship, $array, $column) {
            $query->whereIn($relationship . "." . $column, $array);
        });
    }

    /**
     * Scopes the query to results that reference given key.
     *
     * @param  Builder  $query
     * @param  string  $key
     * @param  int|array  $value
     * @return Builder
     */
    private function scopeReferencingIntegerKey(Builder $query, string $key, $value): Builder
    {
        // First we check if unit id was given
        if ($value === 0) {
            return $query;
        }

        // We apply the scope for array
        if (is_array($value)) {
            return $query->orWhereIn($key, $value);
        }

        // We apply the scope for single value
        return $query->orWhere($key, $value);
    }

    /**
     * Scopes the query to results that share resource relationship.
     *
     * @param  Builder  $query
     * @param  string  $relationship
     * @param  string  $column
     * @return Builder
     */
    private function scopeCommonRelationship(Builder $query, string $relationship, string $column = "id"): Builder
    {
        // We get authorized user's related resource as an array of ids
        $resource = auth()
            ->user()
            ->{$relationship}
            ->pluck($column)
            ->toArray();

        // We check if the model that this scope is used on is actually the model
        // referring to the relationship. For example, we may check relationship
        // with "units" on Unit model which is invalid. In such case, we need to
        // check the resource relationship against current model's id.
        if ($this->getTable() === $relationship) {
            return $query->orWhereIn($column, $resource);
        }

        // We apply the scope
        return $query->orWhereHas($relationship, function (Builder $query) use ($relationship, $resource, $column) {
            $query->whereIn($relationship . "." . $column, $resource);
        });
    }
}
