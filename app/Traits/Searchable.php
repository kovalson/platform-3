<?php
declare(strict_types=1);
namespace App\Traits;

use App\Http\Helpers\RequestKey;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

trait Searchable
{
    /**
     * Scopes the results by search phrase using given columns array.
     * Also merges that array with columns stored in class property.
     *
     * @see Searchable::scopeSearched()
     * @param  Builder  $query
     * @param  array  $columns
     * @param  string  $searchPhrase
     * @return Builder
     */
    public function scopeSearchedWithColumns(Builder $query, array $columns, string $searchPhrase = ""): Builder
    {
        // We check if there is a property storing columns to search by
        // and if so we merge these columns with the ones passed as argument
        if (isset($this->searchColumns) && is_array($this->searchColumns)) {
            $columns = array_merge($columns, $this->searchColumns);
        }

        // We return the scoped query
        return $this->scopeSearched($query, $columns, $searchPhrase);
    }

    /**
     * Scopes the results by search phrase.
     *
     * @param  Builder  $query
     * @param  array  $columns
     * @param  string  $searchPhrase
     * @return Builder
     */
    public function scopeSearched(Builder $query, array $columns = [], string $searchPhrase = ""): Builder
    {
        // First we get the columns passed as argument and if these are empty
        // we take the ones that come from the class property
        if (empty($columns) && isset($this->searchColumns) && is_array($this->searchColumns)) {
            $columns = $this->searchColumns;
        }

        // First we get the search phrase passed as argument and if it is empty
        // we try to take it from the request object
        if (trim($searchPhrase) === "") {
            $searchPhrase = request()->input(RequestKey::SEARCH_QUERY) ?? "";
        }

        // If there are no columns to search through or the search phrase
        // is empty we leave as there is nothing to do
        if (empty($columns) || trim($searchPhrase) === "") {
            return $query;
        }

        // We need to wrap the whole query with closure as there
        // might be other queries
        return $query->where(function (Builder $query) use ($columns, $searchPhrase) {

            // We loop through all columns
            foreach ($columns as $column) {

                // If column relates to other model
                if (Str::contains($column, ".")) {
                    $split = explode(".", $column);
                    $column = array_pop($split);
                    $relationship = join(".", $split);

                    // We go to the relationship using scope
                    $query->orWhereHas($relationship, function (Builder $query) use ($column, $searchPhrase) {

                        // There might be many columns separated with commas for that relationship
                        $columns = explode(",", $column);

                        // We append the search phrase
                        return $query->where(function (Builder $query) use ($columns, $searchPhrase) {
                            foreach ($columns as $columnName) {
                                $query->orWhere($columnName, "LIKE", "%$searchPhrase%");
                            }
                        });

                    });
                } else {

                    // Else it is a column that belongs to the model itself
                    // so we simply append the search phrase
                    $query->orWhere($column, "LIKE", "%$searchPhrase%");

                }

            }

        });
    }
}
