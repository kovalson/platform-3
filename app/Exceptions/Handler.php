<?php
declare(strict_types=1);
namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Exceptions\RoleAlreadyExists;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        "password",
        "password_confirmation",
    ];

    /**
     * Report or log an exception.
     *
     * @param  Throwable  $exception
     * @return void
     *
     * @throws Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request  $request
     * @param  Throwable  $exception
     * @return Response|JsonResponse
     *
     * @throws Throwable
     */
    public function render($request, Throwable $exception)
    {
        $exceptionClass = get_class($exception);

        switch ($exceptionClass) {
            case AuthenticationException::class:
                $message = __("Nie posiadasz wymaganych uprawnień.");
                $code = Response::HTTP_UNAUTHORIZED;
                break;
            case ModelNotFoundException::class:
                $message = __("Nie odnaleziono zasobu.");
                $code = Response::HTTP_NOT_FOUND;
                break;
            case ValidationException::class:
                /** @var ValidationException $exception */
                $message = $exception->errors();
                $code = Response::HTTP_UNPROCESSABLE_ENTITY;
                break;
            case RoleAlreadyExists::class:
                $message = __("Istnieje już rola o takiej nazwie.");
                $code = Response::HTTP_CONFLICT;
                break;
            default:
                $message = $exception->getMessage();
                $code = $exception->getCode();
                break;
        }

        // We need to make sure that the error code is correct
        // or correct it by ourselves
        if ($code <= 1) {
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return response()->json($message, $code);
    }
}
