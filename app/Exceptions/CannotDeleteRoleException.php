<?php
declare(strict_types=1);
namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;
use Throwable;

class CannotDeleteRoleException extends Exception
{
    /**
     * CannotDeleteRoleException constructor.
     *
     * @param  Throwable|null  $previous
     */
    public function __construct(Throwable $previous = null)
    {
        parent::__construct(
            __("Nie można usunąć tej roli."),
            Response::HTTP_FORBIDDEN,
            $previous
        );
    }
}
