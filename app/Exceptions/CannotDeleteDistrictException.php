<?php
declare(strict_types=1);
namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class CannotDeleteDistrictException extends Exception
{
    /**
     * Returns new CannotDeleteDistrictException for a case where
     * the district has scouts or units related to it.
     *
     * @return CannotDeleteDistrictException
     */
    public static function hasScoutsOrUnits(): CannotDeleteDistrictException
    {
        return new CannotDeleteDistrictException(
            __("Nie można usunąć hufca, ponieważ posiada przyłączone jednostki lub harcerzy."),
            Response::HTTP_CONFLICT
        );
    }
}
