<?php
declare(strict_types=1);
namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class UnauthorizedResourceActionException extends Exception
{
    /**
     * UnauthorizedResourceActionException constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct(
            __("Nie posiadasz uprawnień, aby wykonać tę akcję."),
            Response::HTTP_UNAUTHORIZED
        );
    }
}
