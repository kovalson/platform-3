<?php
declare(strict_types=1);
namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class CannotDeleteUnitException extends Exception
{
    /**
     * Returns new CannotDeleteDistrictException for a case where
     * the district has scouts or units related to it.
     *
     * @return CannotDeleteUnitException
     */
    public static function hasScouts(): CannotDeleteUnitException
    {
        return new CannotDeleteUnitException(
            __("Nie można usunąć hufca, ponieważ posiada przyłączonych harcerzy."),
            Response::HTTP_CONFLICT
        );
    }
}
