<?php
declare(strict_types=1);
namespace App\Exceptions;

use Exception;

class SettingNotFoundException extends Exception
{
    /**
     * SettingNotFoundException constructor.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct(__("Nie znaleziono ustawienia."), 404);
    }
}
