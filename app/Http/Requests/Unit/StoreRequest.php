<?php
declare(strict_types=1);
namespace App\Http\Requests\Unit;

use App\Http\Requests\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $districtId = $this->input("district_id");

        return [
            "name" => [
                "required",
                "string",
                Rule::unique("units")->where("district_id", $districtId)
            ],
            "district_id" => "required|exists:districts,id"
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "name.required" => __("Należy podać nazwę jednostki."),
            "name.string" => __("Nazwa jednostki musi być ciągiem znaków."),
            "name.unique" => __("Taka jednostka już istnieje w tym hufcu."),
            "district_id.required" => __("Należy wybrać hufiec, do którego należy jednostka."),
            "district_id.exists" => __("Wybrany hufiec nie istnieje.")
        ];
    }
}
