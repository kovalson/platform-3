<?php
declare(strict_types=1);
namespace App\Http\Requests\Account;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "current_password" => "required|password",
            "new_password" => "required|confirmed"
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "current_password.required" => __("Należy podać aktualne hasło."),
            "current_password.password" => __("Aktualne hasło jest nieprawidłowe."),
            "new_password.required" => __("Należy podać nowe hasło."),
            "new_password.confirmed" => __("Nowe hasło oraz jego potwierdzenie muszą być identyczne.")
        ];
    }
}
