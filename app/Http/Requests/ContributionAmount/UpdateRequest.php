<?php
declare(strict_types=1);
namespace App\Http\Requests\ContributionAmount;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "amounts" => "required|array",
            "amounts.*.year" => "required|integer|distinct",
            "amounts.*.quarter1" => "required|numeric|min:0",
            "amounts.*.quarter2" => "required|numeric|min:0",
            "amounts.*.quarter3" => "required|numeric|min:0",
            "amounts.*.quarter4" => "required|numeric|min:0"
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "amounts.required" => __("Należy podać tablicę kwot składek."),
            "amounts.array" => __("Kwoty składek muszą być tablicą elementów."),
            "amounts.*.year.required" => __("Należy podać rok kwoty składki."),
            "amounts.*.year.integer" => __("Rok kwoty składki musi być liczbą całkowitą."),
            "amounts.*.year.distinct" => __("Rok kwoty składki nie może się powielać."),
            "amounts.*.quarter1.required" => __("Należy podać kwotę składki za pierwszy kwartał."),
            "amounts.*.quarter1.numeric" => __("Kwota składki za pierwszy kwartał musi być liczbą."),
            "amounts.*.quarter1.min" => __("Kwota składki za pierwszy kwartał nie może być mniejsza niż :min."),
            "amounts.*.quarter2.required" => __("Należy podać kwotę składki za drugi kwartał."),
            "amounts.*.quarter2.numeric" => __("Kwota składki za drugi kwartał musi być liczbą."),
            "amounts.*.quarter2.min" => __("Kwota składki za drugi kwartał nie może być mniejsza niż :min."),
            "amounts.*.quarter3.required" => __("Należy podać kwotę składki za trzeci kwartał."),
            "amounts.*.quarter3.numeric" => __("Kwota składki za trzeci kwartał musi być liczbą."),
            "amounts.*.quarter3.min" => __("Kwota składki za trzeci kwartał nie może być mniejsza niż :min."),
            "amounts.*.quarter4.required" => __("Należy podać kwotę składki za czwarty kwartał."),
            "amounts.*.quarter4.numeric" => __("Kwota składki za czwarty kwartał musi być liczbą."),
            "amounts.*.quarter4.min" => __("Kwota składki za czwarty kwartał nie może być mniejsza niż :min.")
        ];
    }
}
