<?php
declare(strict_types=1);
namespace App\Http\Requests\Auth;

use App\Http\Requests\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email" => "required|email|exists:users,email",
            "password" => "required"
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "email.required" => __("Należy podać adres e-mail"),
            "email.email" => __("Podany adres nie jest prawidłowym adresem e-mail"),
            "email.exists" => __("Podany adres e-mail nie został znaleziony"),
            "password.required" => __("Należy podać hasło")
        ];
    }
}
