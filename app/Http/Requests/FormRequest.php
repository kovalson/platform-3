<?php
declare(strict_types=1);
namespace App\Http\Requests;

use App\Exceptions\UnauthorizedResourceActionException;
use App\Traits\Scopeable;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest as LaravelFormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

/**
 * Class FormRequest
 * This class helps returning JSON response instead redirecting to 404 page when
 * invalid request data was sent.
 *
 * @package App\Http\Requests
 */
abstract class FormRequest extends LaravelFormRequest
{
    /**
     * Checks whether user is permitted to process given input values.
     *
     * @param  Scopeable|Model  $model
     * @param  array  $inputs
     * @throws UnauthorizedResourceActionException
     */
    public function checkResourcesAccess($model, array $inputs): void
    {
        foreach ($inputs as $input => $permission) {
            if ($this->has($input)) {
                $model->checkResourceActionAccess($permission);
            }
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract public function rules();

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    abstract public function authorize();

    /**
     * Handle a failed validation attempt.
     *
     * @param  Validator  $validator
     * @return  void
     * @throws  HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        // We get the errors from the validation
        $errors = (new ValidationException($validator))->errors();

        // We throw new error that returns JSON response
        throw new HttpResponseException(
            response()->json(["errors" => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }

    /**
     * Handle a failed authorization attempt.
     *
     * @return  void
     * @throws  AuthorizationException
     */
    protected function failedAuthorization()
    {
        throw new AuthorizationException(
            response()->json()
        );
    }
}
