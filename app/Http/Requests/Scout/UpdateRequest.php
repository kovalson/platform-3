<?php
declare(strict_types=1);
namespace App\Http\Requests\Scout;

use App\Models\Scout;

class UpdateRequest extends StoreRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        /** @var Scout $scout */
        $scout = $this->route("scout");

        $rules["last_name"] = "sometimes|" . $rules["last_name"];
        $rules["first_name"] = "sometimes|" . $rules["first_name"];
        $rules["registration_number"] = "sometimes|" . $rules["registration_number"] . "," . $scout->id;

        return $rules;
    }
}
