<?php
declare(strict_types=1);
namespace App\Http\Requests\Scout;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->has("registration_number")) {
            $user = auth()->user();

            return $user->can(config("permissions.scout.registration_number.edit"));
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "last_name" => "required|string",
            "first_name" => "required|string",
            "registration_number" => "required|regex:/[A-Z]{2}[0-9]{9}/|unique:scouts,registration_number",
            "unit_id" => "nullable|exists:units,id",
            "district_id" => "nullable|exists:districts,id",
            "joined_at" => "nullable|date_format:Y-m-d",
            "quit_at" => "nullable|date_format:Y-m-d",
            "remarks" => "nullable|string"
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "last_name.required" => __("Należy podać nazwisko harcerza."),
            "last_name.string" => __("Nazwisko harcerza musi być ciągiem znaków."),
            "first_name.required" => __("Należy podać imię harcerza."),
            "first_name.string" => __("Imię harcerza musi być ciągiem znaków."),
            "registration_number.required" => __("Należy podać numer ewidencyjny."),
            "registration_number.regex" => __("Nieprawidłowy format numeru ewidencyjnego."),
            "registration_number.unique" => __("Podany numer ewidencyjny już istnieje."),
            "unit_id.exists" => __("Wybrana jednostka nie istnieje."),
            "district_id.exists" => __("Wybrany hufiec nie istnieje."),
            "joined_at.date_format" => __("Nieprawidłowy format daty dołączenia do ZHP."),
            "quit_at.date_format" => __("Nieprawidłowy format daty wystąpienia z ZHP."),
            "remarks.string" => __("Uwagi muszą być ciągiem znaków.")
        ];
    }
}
