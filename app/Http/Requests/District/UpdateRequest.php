<?php
declare(strict_types=1);
namespace App\Http\Requests\District;

use App\Http\Requests\FormRequest;
use App\Models\District;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /* @var District $district */
        $district = $this->route("district");

        return [
            "name" => "sometimes|string|unique:districts,name," . $district->id,
            "last_modified" => "sometimes|date_format:Y-m-d H:i:s",
            "transfer_details" => "sometimes|nullable|string"
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "name.string" => __("Nazwa hufca musi być ciągiem znaków."),
            "name.unique" => __("Istnieje już hufiec o takiej nazwie."),
            "last_modified.date_format" => __("Data ostatniej modyfikacji ma nieprawidłowy format."),
            "transfer_details.string" => __("Szczegóły przelewu muszą być ciągiem znaków.")
        ];
    }
}
