<?php
declare(strict_types=1);
namespace App\Http\Requests\District;

use App\Http\Requests\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|string|unique:districts,name",
            "transfer_details" => "nullable|string"
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "name.required" => __("Należy podać nazwę hufca."),
            "name.string" => __("Nazwa hufca musi być ciągiem znaków."),
            "name.unique" => __("Istnieje już hufiec o takiej nazwie."),
            "transfer_details.string" => __("Szczegóły przelewu muszą być ciągiem znaków.")
        ];
    }
}
