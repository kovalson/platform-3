<?php
declare(strict_types=1);
namespace App\Http\Requests\Contribution;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // We get the currently authorized user
        $user = auth()->user();

        return $user->canEditScoutContributions($this->input("scout_id"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "scout_id" => "required|exists:scouts,id",
            "year" => "required|integer",
            "quarter" => "required|integer|in:1,2,3,4",
            "paid" => "required|boolean"
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "scout_id.required" => __("Należy podać harcerza."),
            "scout_id.exists" => __("Nie znaleziono harcerza."),
            "year.required" => __("Należy podać rok składki."),
            "year.integer" => __("Rok składki musi być liczbą."),
            "quarter.required" => __("Należy podać kwartał składki."),
            "quarter.integer" => __("Kwartał składki musi być liczbą."),
            "quarter.in" => __("Nieprawidłowy kwartał składki."),
            "paid.required" => __("Należy podać informację, czy składka jest opłacona czy nie."),
            "paid.boolean" => __("Informacja o opłaceniu składki musi być wartością logiczną 0-1.")
        ];
    }
}
