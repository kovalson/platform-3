<?php
declare(strict_types=1);
namespace App\Http\Requests\Role;

use App\Http\Requests\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class PutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $scopesFlatArray = array_values(Arr::flatten(config("role_permission_scopes")));

        return [
            "name" => "required",
            "permissions" => "present|array",
            "permissions.*" => "exists:permissions,name",
            "scopes_permissions" => "present|array",
            "scopes_permissions.*" => "exists:permissions,name",
            "scopes_scopes" => "present|array",
            "scopes_scopes.*" => Rule::in($scopesFlatArray),
            "scopes_values" => "present|array",
            "scopes_values.*" => "array",
            "scopes_values.*.*" => "integer"
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "name.required" => __("Należy podać nazwę roli."),
            "permissions.present" => __("Nie podano tablicy pozwoleń."),
            "permissions.array" => __("Pozwolenia muszą być tablicą wartości."),
            "permissions.*.exists" => __("Jedno z podanych pozwoleń nie istnieje."),
            "scopes_permissions.present" => __("Nie podano tablicy zasięgów (brak wartości pozwoleń)."),
            "scopes_permissions.array" => __("Zasięgi muszą być tablicą wartości (wartości pozwoleń)."),
            "scopes_permissions.*.exists" => __("Pozwolenie określone dla jednego z zasięgów nie istnieje."),
            "scopes_scopes.present" => __("Nie podano tablicy zasięgów (brak wartości zasięgów)."),
            "scopes_scopes.array" => __("Zasięgi muszą być tablicą wartości (wartości zasięgów)."),
            "scopes_scopes.*.in" => __("Jeden z zasięgów jest nieprawidłowo określony."),
            "scopes_values.present" => __("Nie podano tablicy zasięgów (brak wartości)."),
            "scopes_values.array" => __("Zasięgi muszą być tablicą tablic wartości (wartości)."),
            "scopes_values.*.array" => __("Tablica wartości zasięgów musi być tablicą liczb."),
            "scopes_values.*.*.integer" => __("Wszystkie wartości zasięgów muszą być liczbami.")
        ];
    }
}
