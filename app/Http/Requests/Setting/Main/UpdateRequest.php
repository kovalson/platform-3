<?php
declare(strict_types=1);
namespace App\Http\Requests\Setting\Main;

use App\Http\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "settings" => "required|array",
            "settings.*.name" => "required|string|exists:settings,name",
            "settings.*.value" => "required|string"
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "settings.required" => __("Należy podać modyfikowane ustawienia."),
            "settings.array" => __("Ustawienia muszą być tablicą elementów."),
            "settings.*.name.required" => __("Należy określić nazwę modyfikowanego ustawienia."),
            "settings.*.name.string" => __("Nazwa modyfikowanego ustawienia musi być ciągiem znaków."),
            "settings.*.name.exists" => __("Modyfikowane ustawienie nie istnieje."),
            "settings.*.value.required" => __("Należy określić nową wartość ustawienia."),
            "settings.*.value.string" => __("Wartość ustawienia musi być ciągiem znaków.")
        ];
    }
}
