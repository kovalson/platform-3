<?php
declare(strict_types=1);
namespace App\Http\Requests\User;

use App\Http\Requests\FormRequest;
use App\Models\User;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /** @var User $user */
        $user = $this->route("user");

        return [
            "name" => "required|string",
            "email" => "required|email|unique:users,email," . $user->id,
            "password" => "nullable|confirmed",
            "roles" => "present|array",
            "roles.*" => "integer|exists:roles,id",
            "districts" => "present|array",
            "districts.*" => "integer|exists:districts,id",
            "units" => "present|array",
            "units.*" => "integer|exists:units,id"
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            "name.required" => __("Należy podać nazwę użytkownika."),
            "name.string" => __("Nazwa użytkownika musi być ciągiem znaków."),
            "email.required" => __("Należy podać adres e-mail."),
            "email.email" => __("Adres e-mail musi być prawidłowym adresem e-mail."),
            "email.unique" => __("Podany adres e-mail już istnieje."),
            "password.required" => __("Należy podać hasło nowego użytkownika."),
            "password.confirmed" => __("Podane hasła muszą być identyczne."),
            "roles.present" => __("Należy podać tablicę ról."),
            "roles.array" => __("Role muszą być tablicą wartości."),
            "roles.*.integer" => __("Identyfikator roli musi być liczbą."),
            "roles.*.exists" => __("Jedna z podanych ról nie istnieje."),
            "districts.present" => __("Należy podać tablicę hufców."),
            "districts.array" => __("Hufce muszą być tablicą wartości."),
            "districts.*.integer" => __("Identyfikator hufca musi być liczbą."),
            "districts.*.exists" => __("Jeden z podanych hufców nie istnieje."),
            "units.present" => __("Należy podać tablicę jednostek."),
            "units.array" => __("Jednostki muszą być tablicą wartości."),
            "units.*.integer" => __("Identyfikator jednostki musi być liczbą."),
            "units.*.exists" => __("Jedna z podanych jednostek nie istnieje.")
        ];
    }
}
