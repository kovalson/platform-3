<?php
declare(strict_types=1);
namespace App\Http\Helpers;

/**
 * Class RequestKey
 * Helper class storing request keys names.
 *
 * @package App\Http\Helpers
 */
class RequestKey
{
    /**
     * Key used to find scopes for given model permission.
     *
     * @type string
     */
    const MODEL_PERMISSION = "model_permission";

    /**
     * Key used to get number of results per page during the pagination.
     *
     * @type string
     */
    const PER_PAGE = "per_page";

    /**
     * The search query key.
     *
     * @type string
     */
    const SEARCH_QUERY = "q";
}
