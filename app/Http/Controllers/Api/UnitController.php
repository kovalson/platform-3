<?php
declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Exceptions\UnauthorizedResourceActionException;
use App\Http\Controllers\Controller;
use App\Http\Helpers\RequestKey;
use App\Http\Requests\Unit\StoreRequest;
use App\Http\Requests\Unit\UpdateRequest;
use App\Models\District;
use App\Models\Unit;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;

class UnitController extends Controller
{
    /**
     * UnitController constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->permissionMiddlewares([
            "create" => config("permissions.unit.create"),
            "store" => config("permissions.unit.create"),
            "edit" => config("permissions.unit.edit"),
            "update" => config("permissions.unit.edit"),
            "destroy" => config("permissions.unit.delete")
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginatedUnits = Unit::searched()
            ->filtered()
            ->sorted()
            ->scoped(config("permissions.unit.access"))
            ->with("district")
            ->withCount(["users", "scouts"])
            ->paginate($request->input(RequestKey::PER_PAGE));

        return response()->paginated($paginatedUnits, "units");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        $districtId = $request->input("district_id");

        try {
            // We wrap everything with a transaction as there are exceptions
            // that might be thrown
            DB::beginTransaction();

            // We check if user has rights to assign unit the given district
            District::find($districtId)->checkResourceActionAccess(config("permissions.unit.assign.district"));

            // We create the unit
            Unit::create([
                "name" => $request->input("name"),
                "district_id" => $request->input("district_id")
            ]);

            // We commit the data
            DB::commit();
        } catch (Throwable $exception) {
            try {
                DB::rollBack();
            } catch (Throwable $exception) {
                return response()->json($this->getRollbackFailureMessage(), 500);
            }

            return response()->json($exception->getMessage(), $exception->getCode());
        }

        return response()->json(__("Jednostka została dodana."));
    }

    /**
     * Display the specified resource.
     *
     * @param  Unit  $unit
     * @return JsonResponse
     */
    public function show(Unit $unit): JsonResponse
    {
        $unit->load(["district"]);

        return response()->json($unit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Unit  $unit
     * @return JsonResponse
     * @throws UnauthorizedResourceActionException
     */
    public function edit(Unit $unit): JsonResponse
    {
        $unit->checkResourceActionAccess(config("permissions.unit.edit"));

        $unit->load("district");

        return response()->json($unit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Unit  $unit
     * @return JsonResponse
     * @throws UnauthorizedResourceActionException
     */
    public function update(UpdateRequest $request, Unit $unit): JsonResponse
    {
        $request->checkResourcesAccess($unit, [
            "name" => config("permissions.unit.edit"),
            "district_id" => config("permissions.unit.assign.district")
        ]);

        $attributes = $request->only([
            "name",
            "district_id"
        ]);

        $unit->update($attributes);

        return response()->json("Dane jednostki zostały zapisane.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Unit  $unit
     * @return JsonResponse
     */
    public function destroy(Unit $unit): JsonResponse
    {
        try {
            $unit->delete();
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), $exception->getCode());
        }

        return response()->json(__("Jednostka została usunięta."));
    }
}
