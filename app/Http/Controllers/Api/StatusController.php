<?php
declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Scout;
use Illuminate\Http\JsonResponse;

class StatusController extends Controller
{
    /**
     * Get the contributions status for a scout.
     *
     * @param  string  $registrationNumber
     * @return JsonResponse
     */
    public function index(string $registrationNumber): JsonResponse
    {
        // We try to find the scout with given registration number
        $scout = Scout::where("registration_number", $registrationNumber)
            ->with(["unit", "unit.district", "district"])
            ->first();

        // If the scout exists we retrieve his contributions profile
        // and return it as a response along with contributions amounts
        // and other information.
        if ($scout instanceof Scout) {
            $status = $scout->getContributionsStatus();

            return response()->json([
                "status" => $status
            ]);
        }

        // Else the scout was not found and we return an error
        return response()->json(__("Nie znaleziono harcerza."), 404);
    }
}
