<?php
declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Account\UpdateRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    /**
     * Updates the account data.
     *
     * @param  UpdateRequest  $request
     * @return JsonResponse
     */
    public function update(UpdateRequest $request): JsonResponse
    {
        // We get the currently authorized user
        $user = auth()->user();

        $user->update([
            "password" => Hash::make($request->input("new_password"))
        ]);

        return response()->json(__("Hasło zostało zaktualizowane."));
    }
}
