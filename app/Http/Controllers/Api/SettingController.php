<?php
declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\Main\UpdateRequest;
use App\Models\Setting;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Throwable;

class SettingController extends Controller
{
    /**
     * SettingController constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->permissionMiddlewares([
            "update" => config("permissions.setting.contributions.edit"),
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $settings = Setting::all();

        return response()->json([
            "settings" => $settings
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @return JsonResponse
     */
    public function update(UpdateRequest $request)
    {
        // We get the settings array passed as parameter
        $settings = $request->input("settings");

        try {
            // We wrap everything with a transaction as something
            // might go wrong
            DB::beginTransaction();

            // We loop through all settings and update each one
            foreach ($settings as $setting) {
                Setting::where("name", $setting["name"])->update([
                    "value" => $setting["value"]
                ]);
            }

            // We commit the changes
            DB::commit();
        } catch (Throwable $exception) {
            try {
                DB::rollBack();

                return $this->rollbackFailureResponse();
            } catch (Throwable $exception) {
                return $this->exceptionResponse($exception);
            }
        }

        return response()->json(__("Ustawienia zostały zapisane."));
    }
}
