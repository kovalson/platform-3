<?php
declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Exceptions\UnauthorizedResourceActionException;
use App\Http\Controllers\Controller;
use App\Http\Helpers\RequestKey;
use App\Http\Requests\Scout\StoreRequest;
use App\Http\Requests\Scout\UpdateRequest;
use App\Models\District;
use App\Models\Scout;
use App\Models\Unit;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;

class ScoutController extends Controller
{
    /**
     * RoleController constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->permissionMiddlewares([
            "store" => config("permissions.scout.create"),
            "update" => config("permissions.scout.edit"),
            "destroy" => config("permissions.scout.delete")
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        // We get the currently authorized user
        $user = auth()->user();

        // We set the additional registration_number column if
        // the user is authorized to search by it
        $additionalColumns = $user->can(config("permissions.scout.registration_number.display"))
            ? ["registration_number"]
            : [];

        // We set the additional archived column if
        // the user is authorized to filter by it
        $additionalFilters = $user->can(config("permissions.scout.archived.access"))
            ? ["archived"]
            : [];

        $paginatedScouts = Scout::searchedWithColumns($additionalColumns)
            ->filteredWithColumns($additionalFilters)
            ->sorted()
            ->scoped(config("permissions.scout.access"))
            ->with(["district", "unit"])
            ->paginate($request->input(RequestKey::PER_PAGE));

        return response()->paginated($paginatedScouts, "scouts");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        try {
            // We wrap everything with a transaction as there are exceptions
            // that might be thrown
            DB::beginTransaction();

            // We create the scout data
            $data = $this->createScoutDataFromRequest($request);

            // We create a new scout
            Scout::create($data);

            // We commit the data
            DB::commit();
        } catch (Throwable $exception) {
            try {
                DB::rollBack();
            } catch (Throwable $exception) {
                return $this->rollbackFailureResponse();
            }

            return $this->exceptionResponse($exception);
        }

        return response()->json(__("Harcerz został dodany."));
    }

    /**
     * Display the specified resource.
     *
     * @return JsonResponse
     */
    public function show(): JsonResponse
    {
        return $this->unsupportedRouteResponse();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Scout  $scout
     * @return JsonResponse
     * @throws UnauthorizedResourceActionException
     */
    public function edit(Scout $scout): JsonResponse
    {
        $scout->checkResourceActionAccess(config("permissions.scout.edit"));

        $scout->load(["unit", "district"]);

        return response()->json($scout);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Scout  $scout
     * @return JsonResponse
     */
    public function update(UpdateRequest $request, Scout $scout): JsonResponse
    {
        try {
            // We wrap everything with a transaction as there are exceptions
            // that might be thrown
            DB::beginTransaction();

            // We create the scout data
            $data = $this->createScoutDataFromRequest($request);

            // We create a new scout
            $scout->update($data);

            // We commit the data
            DB::commit();
        } catch (Throwable $exception) {
            try {
                DB::rollBack();
            } catch (Throwable $exception) {
                return $this->rollbackFailureResponse();
            }

            return $this->exceptionResponse($exception);
        }

        return response()->json(__("Dane harcerza zostały zapisane."));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Scout  $scout
     * @return JsonResponse
     */
    public function destroy(Scout $scout): JsonResponse
    {
        try {
            $scout->delete();
        } catch (Throwable $exception) {
            return $this->exceptionResponse($exception);
        }

        return response()->json(__("Harcerz został trwale usunięty."));
    }

    /**
     * Creates scout data to store or update from the request object.
     *
     * @param  StoreRequest|UpdateRequest  $request
     * @return array
     * @throws UnauthorizedResourceActionException
     */
    private function createScoutDataFromRequest($request): array
    {
        $districtId = $request->input("district_id");
        $unitId = $request->input("unit_id");

        // We check the current user's permission to assign unit to a scout
        if ($unitId) {
            Unit::find($unitId)->checkResourceActionAccess(config("permissions.scout.assign.unit"));
        }

        // We check the current user's permission to assign district to a scout
        if ($districtId) {
            District::find($districtId)->checkResourceActionAccess(config("permissions.scout.assign.district"));
        }

        // We create the data array
        $data = $request->only([
            "last_name",
            "first_name",
            "registration_number",
            "unit_id",
            "district_id",
            "joined_at",
            "quit_at",
            "remarks"
        ]);

        // We append the archived flag (if present)
        $data["archived"] = $request->boolean("archived");

        // If the unit is specified - we try to take its district
        // as a scout's district instead of the given one. If the unit
        // has no district we take the given one.
        if (isset($data["unit_id"])) {
            $unit = Unit::find($data["unit_id"]);

            if ($unit->district) {
                $data["district_id"] = $unit->district->id;
            }
        }

        // We return the created data
        return $data;
    }
}
