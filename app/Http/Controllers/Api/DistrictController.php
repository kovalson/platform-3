<?php
declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Exceptions\UnauthorizedResourceActionException;
use App\Http\Controllers\Controller;
use App\Http\Helpers\RequestKey;
use App\Http\Requests\District\StoreRequest;
use App\Http\Requests\District\UpdateRequest;
use App\Models\District;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    /**
     * RoleController constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->permissionMiddlewares([
            "store" => config("permissions.district.create"),
            "show" => config("permissions.district.access"),
            "edit" => config("permissions.district.edit"),
            "update" => config("permissions.district.edit"),
            "destroy" => config("permissions.district.delete")
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginatedDistricts = District::searched()
            ->sorted()
            ->scoped(config("permissions.district.access"))
            ->withCount(["users", "units", "scouts"])
            ->paginate($request->input(RequestKey::PER_PAGE));

        return response()->paginated($paginatedDistricts, "districts");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        $data = $request->only([
            "name",
            "transfer_details"
        ]);

        District::create($data);

        return response()->json(__("Hufiec został dodany."));
    }

    /**
     * Display the specified resource.
     *
     * @param  District  $district
     * @return JsonResponse
     */
    public function show(District $district): JsonResponse
    {
        $district->loadCount(["users", "units", "scouts"]);

        return response()->json($district);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  District  $district
     * @return JsonResponse
     * @throws UnauthorizedResourceActionException
     */
    public function edit(District $district): JsonResponse
    {
        $district->checkResourceActionAccess(config("permissions.district.edit"));

        return response()->json($district);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  District  $district
     * @return JsonResponse
     * @throws UnauthorizedResourceActionException
     */
    public function update(UpdateRequest $request, District $district): JsonResponse
    {
        $request->checkResourcesAccess($district, [
            "name" => config("permissions.district.edit"),
            "last_modified" => config("permissions.district.last_modified.edit"),
            "transfer_details" => config("permissions.district.transfer_details.edit")
        ]);

        $attributes = $request->only([
            "name",
            "last_modified",
            "transfer_details"
        ]);

        $district->update($attributes);

        return response()->json(__("Dane hufca zostały zapisane."));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  District  $district
     * @return JsonResponse
     */
    public function destroy(District $district): JsonResponse
    {
        try {
            $district->delete();
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), $exception->getCode());
        }

        return response()->json(__("Hufiec został usunięty."));
    }
}
