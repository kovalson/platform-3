<?php
declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Helpers\RequestKey;
use App\Http\Requests\Role\StoreRequest;
use App\Http\Requests\Role\UpdateRequest;
use App\Models\Role;
use App\Models\RolePermissionScope;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Throwable;

class RoleController extends Controller
{
    /**
     * RoleController constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->permissionMiddlewares([
            "store" => config("permissions.role.create"),
            "edit" => config("permissions.role.edit"),
            "update" => config("permissions.role.edit"),
            "destroy" => config("permissions.role.delete")
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $paginatedRoles = Role::searched()
            ->sorted()
            ->scoped()
            ->withCount(["users", "permissions", "permissionsScopes"])
            ->paginate($request->input(RequestKey::PER_PAGE));

        return response()->paginated($paginatedRoles, "roles");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create()
    {
        return response()->json($this->getUnsupportedRouteMessage(), 500);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request)
    {
        try {
            // We wrap everything with a transaction as there are exceptions
            // that might be thrown
            DB::beginTransaction();

            // We create the role
            $role = Role::create([
                "name" => $request->input("name")
            ]);

            // We append permissions to the role
            $role->givePermissionTo($request->input("permissions"));

            // We create permissions scopes
            $this->createRolePermissionsScopes($role->id, $request);

            // We commit successful transaction
            DB::commit();
        } catch (Throwable $exception) {
            try {
                DB::rollBack();
            } catch (Throwable $exception) {
                return response()->json($this->getRollbackFailureMessage(), 500);
            }

            return response()->json($exception->getMessage(), $exception->getCode());
        }

        return response()->json(__("Rola została dodana."));
    }

    /**
     * Display the specified resource.
     *
     * @return JsonResponse
     */
    public function show()
    {
        return response()->json($this->getUnsupportedRouteMessage(), 500);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Role  $role
     * @return JsonResponse
     */
    public function edit(Role $role)
    {
        $role->load([
            "permissions",
            "permissionsScopes",
            "permissionsScopes.permission"
        ]);

        return response()->json($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  Role  $role
     * @return JsonResponse
     */
    public function update(UpdateRequest $request, Role $role)
    {
        try {
            // We wrap everything with transaction as something might go wrong
            DB::beginTransaction();

            $role->update([
                "name" => $request->input("name")
            ]);

            // We append permissions to the role
            $role->syncPermissions($request->input("permissions"));

            // We remove permissions scopes for that role
            RolePermissionScope::whereRoleId($role->id)->delete();

            // We create permissions scopes
            $this->createRolePermissionsScopes($role->id, $request);

            DB::commit();
        } catch (Throwable $exception) {
            try {
                DB::rollBack();

                return response()->json($exception->getMessage(), $exception->getCode());
            } catch (Throwable $exception) {
                return response()->json($this->getRollbackFailureMessage(), 500);
            }
        }

        return response()->json(__("Rola została zapisana."));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Role  $role
     * @return JsonResponse
     */
    public function destroy(Role $role)
    {
        try {
            $role->delete();
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), $exception->getCode());
        }

        return response()->json(__("Rola została usunięta."));
    }

    /**
     * Creates permission scopes for given role using request data.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return void
     */
    private function createRolePermissionsScopes(int $id, Request $request): void
    {
        $permissions = $request->input("scopes_permissions");
        $scopes = $request->input("scopes_scopes");
        $values = $request->input("scopes_values");

        for ($i = 0; $i < count($permissions); $i++) {
            $scopePermission = $permissions[$i];

            $permission = Permission::findByName($scopePermission);

            RolePermissionScope::create([
                "role_id" => $id,
                "permission_id" => $permission->id,
                "scope" => $scopes[$i],
                "values" => join(",", $values[$i])
            ]);
        }
    }
}
