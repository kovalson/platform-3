<?php
declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contribution\UpdateRequest;
use App\Models\Contribution;
use App\Models\Scout;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Throwable;

class ContributionController extends Controller
{
    /**
     * ContributionController constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->permissionMiddleware(config("permissions.scout.contribution.edit"))->only("update");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @return JsonResponse
     */
    public function update(UpdateRequest $request): JsonResponse
    {
        // We get the request data
        $validated = $request->validated();

        // We try to find the contribution
        $contribution = Contribution::where("scout_id", $validated["scout_id"])
            ->where("year", $validated["year"])
            ->first();

        if ($contribution instanceof Contribution) {
            $contribution->update([
                "quarter" . $validated["quarter"] => $validated["paid"]
            ]);

            // We check if any contribution at all is paid.
            // If not - the entry can be deleted.
            if (!$contribution->isAnyQuarterPaid()) {
                try {
                    $contribution->delete();
                } catch (Throwable $exception) {
                    return response()->json(__("Wystąpił błąd podczas usuwania wpisu składki."), 500);
                }
            }
        } else if ($validated["paid"]) {
            Contribution::create([
                "scout_id" => $validated["scout_id"],
                "year" => $validated["year"],
                "quarter1" => ($validated["quarter"] === 1) ? $validated["paid"] : false,
                "quarter2" => ($validated["quarter"] === 2) ? $validated["paid"] : false,
                "quarter3" => ($validated["quarter"] === 3) ? $validated["paid"] : false,
                "quarter4" => ($validated["quarter"] === 4) ? $validated["paid"] : false
            ]);
        }

        $scout = Scout::find($validated["scout_id"]);

        if ($scout->district) {
            $scout->district->update([
                "last_modified" => Carbon::now()
            ]);
        }

        return response()->json($scout->contributions_profile);
    }
}
