<?php
declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContributionAmount\UpdateRequest;
use App\Models\ContributionAmount;
use App\Models\Setting;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Throwable;

class ContributionAmountController extends Controller
{
    /**
     * ContributionAmountController constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->permissionMiddlewares([
            "index" => config("permissions.setting.contributions_amounts.access"),
            "update" => config("permissions.setting.contributions_amounts.edit"),
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $settings = Setting::getSettingsArray();
        $amounts = ContributionAmount::all();

        return response()->json([
            "settings" => $settings,
            "amounts" => $amounts
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @return JsonResponse
     */
    public function update(UpdateRequest $request)
    {
        // We get the amounts array passed as parameter
        $amounts = $request->input("amounts");

        try {
            // We wrap everything with a transaction as something
            // might go wrong
            DB::beginTransaction();

            // We loop through all amounts and update each one
            foreach ($amounts as $amount) {
                ContributionAmount::updateOrCreate(["year" => $amount["year"]], $amount);
            }

            // We commit the changes
            DB::commit();
        } catch (Throwable $exception) {
            try {
                DB::rollBack();
            } catch (Throwable $exception) {
                return $this->rollbackFailureResponse();
            }
        }

        return response()->json(__("Kwoty składek zostały zapisane."));
    }
}
