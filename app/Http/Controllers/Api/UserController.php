<?php
declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Exceptions\UnauthorizedResourceActionException;
use App\Http\Controllers\Controller;
use App\Http\Helpers\RequestKey;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Throwable;

class UserController extends Controller
{
    /**
     * RoleController constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->permissionMiddlewares([
            "store" => config("permissions.user.create"),
            "update" => config("permissions.user.edit"),
            "destroy" => config("permissions.user.delete")
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $paginatedUsers = User::searched()
            ->sorted()
            ->scoped(config("permissions.user.access"))
            ->others()
            ->with(["roles", "districts", "units"])
            ->withCount(["roles", "districts", "units"])
            ->paginate($request->input(RequestKey::PER_PAGE));

        return response()->paginated($paginatedUsers, "users");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest  $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        $rolesIds = $request->input("roles");
        $districtsIds = $request->input("districts");
        $unitsIds = $request->input("units");

        try {
            // We wrap everything with a transaction as there are exceptions
            // that might be thrown
            DB::beginTransaction();

            // We create a new user
            $user = User::create([
                "email" => $request->input("email"),
                "password" => Hash::make($request->input("password")),
                "name" => $request->input("name"),
                "active" => $request->boolean("active")
            ]);

            // We synchronize all user's relationships
            $user->syncRelationships($rolesIds, $districtsIds, $unitsIds);

            // We commit the data
            DB::commit();
        } catch (Throwable $exception) {
            try {
                DB::rollBack();
            } catch (Throwable $exception) {
                return $this->rollbackFailureResponse();
            }

            return $this->exceptionResponse($exception);
        }

        return response()->json(__("Użytkownik został dodany."));
    }

    /**
     * Display the specified resource.
     *
     * @return JsonResponse
     */
    public function show(): JsonResponse
    {
        return $this->unsupportedRouteResponse();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $user
     * @return JsonResponse
     * @throws UnauthorizedResourceActionException
     */
    public function edit(User $user): JsonResponse
    {
        $user->checkResourceActionAccess(config("permissions.user.edit"));

        $user->load(["roles", "districts", "units"]);

        return response()->json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  User  $user
     * @return JsonResponse
     */
    public function update(UpdateRequest $request, User $user): JsonResponse
    {
        $rolesIds = $request->input("roles");
        $districtsIds = $request->input("districts");
        $unitsIds = $request->input("units");

        try {
            // We wrap everything with a transaction as there are exceptions
            // that might be thrown
            DB::beginTransaction();

            // We prepare the user data to update
            $data = [
                "email" => $request->input("email"),
                "name" => $request->input("name"),
                "active" => $request->boolean("active")
            ];

            // We determine whether to update the password
            if ($request->input("password")) {
                $data["password"] = Hash::make($request->input("password"));
            }

            // We update the data
            $user->update($data);

            // We synchronize all user's relationships
            $user->syncRelationships($rolesIds, $districtsIds, $unitsIds);

            // We commit the data
            DB::commit();
        } catch (Throwable $exception) {
            try {
                DB::rollBack();
            } catch (Throwable $exception) {
                return $this->rollbackFailureResponse();
            }

            return $this->exceptionResponse($exception);
        }

        return response()->json(__("Dane użytkownika zostały zapisane."));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User  $user
     * @return JsonResponse
     */
    public function destroy(User $user): JsonResponse
    {
        try {
            $user->delete();
        } catch (Exception $exception) {
            return $this->exceptionResponse($exception);
        }

        return response()->json(__("Użytkownik został usunięty."));
    }
}
