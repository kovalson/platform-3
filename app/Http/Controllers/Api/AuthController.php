<?php
declare(strict_types=1);
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Name of the user's authentication token.
     */
    const AUTHENTICATION_TOKEN_NAME = "User Authentication Token";

    /**
     * Authorizes user by email and password and returns it's data.
     *
     * @param  LoginRequest  $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        // We get the user with given email address
        $user = User::whereEmail($request->input("email"))->first();

        // We check the password
        if (!Hash::check($request->input("password"), $user->password)) {
            return response()->json(__("Podane hasło jest nieprawidłowe."), Response::HTTP_UNAUTHORIZED);
        }

        // We check if user is active
        if (!$user->active) {
            return response()->json(__("Twoje konto jest nieaktywne."), Response::HTTP_FORBIDDEN);
        }

        // We attempt to authorize the user
        Auth::attempt([
            "email" => $request->input("email"),
            "password" => $request->input("password")
        ]);

        // Get information about last succeeded login
        $lastLoggedIn = $user->last_logged_in;

        // Update last logged in timestamp
        $user->update([
            "last_logged_in" => Carbon::now()
        ]);

        // We create user authentication token
        $token = $user->createToken(self::AUTHENTICATION_TOKEN_NAME)->accessToken;

        // We return user data
        return response()->json([
            "name" => $user->name,
            "token" => $token,
            "last_logged_in" => $lastLoggedIn,
            "roles" => $user->getRolesArray(),
            "permissions" => $user->getPermissionsArray(),
            "scopes" => $user->getPermissionsScopesArray()
        ]);
    }
}
