<?php
declare(strict_types=1);
namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Routing\ControllerMiddlewareOptions;
use Throwable;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    /**
     * Use Spatie "permission" middleware.
     *
     * @param string $permission
     * @return ControllerMiddlewareOptions
     */
    protected function permissionMiddleware(string $permission)
    {
        return $this->middleware("permission:" . $permission);
    }

    /**
     * Use Spatie "permission" middleware for multiple methods passed as an array.
     *
     * @param  array  $permissions
     * @return void
     */
    protected function permissionMiddlewares(array $permissions)
    {
        foreach ($permissions as $method => $permission) {
            $this->permissionMiddleware($permission)->only($method);
        }
    }

    /**
     * Returns the response based on given exception.
     *
     * @param  Throwable  $exception
     * @return JsonResponse
     */
    protected function exceptionResponse(Throwable $exception): JsonResponse
    {
        return response()->json($exception->getMessage(), $exception->getCode());
    }

    /**
     * Returns the response corresponding to rollback failure.
     *
     * @return JsonResponse
     */
    protected function rollbackFailureResponse(): JsonResponse
    {
        return response()->json($this->getRollbackFailureMessage(), 500);
    }

    /**
     * Returns the response corresponding to unsupported route.
     *
     * @return JsonResponse
     */
    protected function unsupportedRouteResponse(): JsonResponse
    {
        return response()->json($this->getUnsupportedRouteMessage(), 500);
    }

    /**
     * Returns message occurring when transaction rollback failed.
     *
     * @return string
     */
    protected function getRollbackFailureMessage(): string
    {
        return __("Wystąpił błąd podczas wycofywania transakcji.");
    }

    /**
     * Returns message occurring when a route is unsupported.
     *
     * @return string
     */
    protected function getUnsupportedRouteMessage(): string
    {
        return __("Ścieżka niewspierana.");
    }
}
