<?php
declare(strict_types=1);
namespace App\Http\Controllers;

use Illuminate\View\View;

class WebRouteController extends Controller
{
    /**
     * Returns the default view for all routes.
     *
     * @return View
     */
    public function index(): View
    {
        return view("layouts.app");
    }
}
