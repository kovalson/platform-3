<?php
declare(strict_types=1);
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Response;

class LanguageController extends Controller
{
    /**
     * Inserts JavaScript snippet including current language localization.
     *
     * @return Response
     */
    public function script()
    {
        // We need to remember this file forever so it doesn't get cleared
        $strings = Cache::rememberForever("lang.js", function () {
            return file_get_contents(resource_path("lang/" . config("app.locale", "pl") . ".json"));
        });

        $headers = [
            "Content-Type" => "text/javascript"
        ];

        $script = "window.i18n = " . $strings . ";";

        return response($script)->withHeaders($headers);
    }
}
