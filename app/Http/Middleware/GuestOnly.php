<?php
declare(strict_types=1);
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class GuestOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->guard("api")->check()) {
            return response()->json(__("Tylko niezalogowani użytkownicy mogą przejść dalej"), 401);
        }

        return $next($request);
    }
}
