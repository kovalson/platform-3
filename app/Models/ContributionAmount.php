<?php
declare(strict_types=1);
namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\ContributionAmount
 *
 * @method static Builder|ContributionAmount newModelQuery()
 * @method static Builder|ContributionAmount newQuery()
 * @method static Builder|ContributionAmount query()
 * @mixin Eloquent
 * @property int $year
 * @property int $quarter1
 * @property int $quarter2
 * @property int $quarter3
 * @property int $quarter4
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|ContributionAmount whereCreatedAt($value)
 * @method static Builder|ContributionAmount whereQuarter1($value)
 * @method static Builder|ContributionAmount whereQuarter2($value)
 * @method static Builder|ContributionAmount whereQuarter3($value)
 * @method static Builder|ContributionAmount whereQuarter4($value)
 * @method static Builder|ContributionAmount whereUpdatedAt($value)
 * @method static Builder|ContributionAmount whereYear($value)
 */
class ContributionAmount extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "contributions_amounts";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "year";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "year",
        "quarter1", "quarter2", "quarter3", "quarter4"
    ];

    /**
     * Creates associative array of contributions amounts.
     *
     * @return array
     */
    public static function getContributionsAmountsArray()
    {
        $contributionsAmounts = ContributionAmount::all();
        $results = [];

        foreach ($contributionsAmounts as $amount) {
            $results[$amount->year] = [
                "quarter1" => (int) $amount->quarter1,
                "quarter2" => (int) $amount->quarter2,
                "quarter3" => (int) $amount->quarter3,
                "quarter4" => (int) $amount->quarter4
            ];
        }

        return $results;
    }
}
