<?php
declare(strict_types=1);
namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\UserDistrict
 *
 * @property int $id
 * @property int $user_id
 * @property int $district_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|UserDistrict newModelQuery()
 * @method static Builder|UserDistrict newQuery()
 * @method static Builder|UserDistrict query()
 * @method static Builder|UserDistrict whereCreatedAt($value)
 * @method static Builder|UserDistrict whereDistrictId($value)
 * @method static Builder|UserDistrict whereId($value)
 * @method static Builder|UserDistrict whereUpdatedAt($value)
 * @method static Builder|UserDistrict whereUserId($value)
 * @mixin Eloquent
 */
class UserDistrict extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "user_id",
        "district_id"
    ];
}
