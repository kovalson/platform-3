<?php
declare(strict_types=1);
namespace App\Models;

use App\Exceptions\CannotDeleteDistrictException;
use App\Traits\Scopeable;
use App\Traits\Searchable;
use App\Traits\Sortable;
use Eloquent;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Carbon;

/**
 * App\Models\District
 *
 * @property int $id
 * @property string $name
 * @property string|null $last_modified
 * @property string|null $transfer_details
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Unit[] $units
 * @property-read int|null $units_count
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|District newModelQuery()
 * @method static Builder|District newQuery()
 * @method static Builder|District query()
 * @method static Builder|District searched($columns = [], $searchPhrase = '')
 * @method static Builder|District sorted($sortColumns = [])
 * @method static Builder|District whereCreatedAt($value)
 * @method static Builder|District whereId($value)
 * @method static Builder|District whereLastModified($value)
 * @method static Builder|District whereName($value)
 * @method static Builder|District whereTransferDetails($value)
 * @method static Builder|District whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Collection|Scout[] $scouts
 * @property-read int|null $scouts_count
 * @method static Builder|District commonDistricts()
 * @method static Builder|District commonRoles()
 * @method static Builder|District commonUnits()
 * @method static Builder|District referencingDistrict($district_id, $referenceKey = 'district_id')
 * @method static Builder|District referencingUnit($unit_id, $referenceKey = 'unit_id')
 * @method static Builder|District relatedToDistricts($districts = [], $relationship = 'districts')
 * @method static Builder|District relatedToUnits($units = [], $relationship = 'units')
 * @method static Builder|District scoped($permission = null)
 * @method static Builder|District withIds($ids = [], $table = '')
 * @method static Builder|District withRoles($roles = [])
 * @method static Builder|District searchedWithColumns($columns, $searchPhrase = '')
 */
class District extends Model
{
    use Searchable;
    use Sortable;
    use Scopeable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name",
        "last_modified",
        "transfer_details"
    ];

    /**
     * The columns to search when filtering model results.
     *
     * @var string[]
     */
    private $searchColumns = [
        "name"
    ];

    /**
     * The columns to sort the results by by default.
     *
     * @var string[]
     */
    private $sortColumns = [
        "name"
    ];

    /**
     * Returns all users related to the district.
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            "user_districts",
            "district_id",
            "user_id"
        );
    }

    /**
     * Returns all units that belong to the district.
     *
     * @return HasMany
     */
    public function units(): HasMany
    {
        return $this->hasMany(
            Unit::class,
            "district_id"
        );
    }

    /**
     * Returns all scouts that belong to units that belong to the district.
     *
     * @return HasManyThrough
     */
    public function scouts(): HasManyThrough
    {
        return $this->hasManyThrough(
            Scout::class,
            Unit::class,
            "district_id",
            "unit_id",
            "id",
            "id"
        );
    }

    /**
     * Delete the model from the database.
     *
     * @return bool|null
     * @throws CannotDeleteDistrictException|Exception
     */
    public function delete()
    {
        // We need to check if the district has any units or scouts
        // If so - the district cannot be deleted.
        if ($this->scouts()->exists() || $this->units()->exists()) {
            throw CannotDeleteDistrictException::hasScoutsOrUnits();
        }

        // If the district has neither units nor scouts assigned
        // we delete it by running the parent delete() method
        return parent::delete();
    }
}
