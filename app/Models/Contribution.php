<?php
declare(strict_types=1);
namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\Contribution
 *
 * @property-read Scout $scout
 * @method static Builder|Contribution newModelQuery()
 * @method static Builder|Contribution newQuery()
 * @method static Builder|Contribution query()
 * @mixin Eloquent
 * @property int $id
 * @property int $scout_id
 * @property string $year
 * @property int $quarter1
 * @property int $quarter2
 * @property int $quarter3
 * @property int $quarter4
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Contribution whereCreatedAt($value)
 * @method static Builder|Contribution whereId($value)
 * @method static Builder|Contribution whereQuarter1($value)
 * @method static Builder|Contribution whereQuarter2($value)
 * @method static Builder|Contribution whereQuarter3($value)
 * @method static Builder|Contribution whereQuarter4($value)
 * @method static Builder|Contribution whereScoutId($value)
 * @method static Builder|Contribution whereUpdatedAt($value)
 * @method static Builder|Contribution whereYear($value)
 */
class Contribution extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "scout_id",
        "year",
        "quarter1", "quarter2", "quarter3", "quarter4"
    ];

    /**
     * Returns relationship with scout it belongs to.
     *
     * @return BelongsTo
     */
    public function scout()
    {
        return $this->belongsTo(Scout::class);
    }

    /**
     * Checks whether any quarter is paid.
     *
     * @return bool
     */
    public function isAnyQuarterPaid(): bool
    {
        return (
            $this->quarter1 ||
            $this->quarter2 ||
            $this->quarter3 ||
            $this->quarter4
        );
    }
}
