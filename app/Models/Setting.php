<?php
declare(strict_types=1);
namespace App\Models;

use App\Exceptions\SettingNotFoundException;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Setting
 *
 * @method static Builder|Setting newModelQuery()
 * @method static Builder|Setting newQuery()
 * @method static Builder|Setting query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property string|null $value
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Setting whereCreatedAt($value)
 * @method static Builder|Setting whereId($value)
 * @method static Builder|Setting whereName($value)
 * @method static Builder|Setting whereUpdatedAt($value)
 * @method static Builder|Setting whereValue($value)
 */
class Setting extends Model
{
    /**
     * Year to start calculating contributions from. (inclusive)
     */
    const CONTRIBUTIONS_YEAR_START = "CONTRIBUTIONS_YEAR_START";

    /**
     * Year to end calculating contributions to. (inclusive)
     */
    const CONTRIBUTIONS_YEAR_END = "CONTRIBUTIONS_YEAR_END";

    /**
     * Last day of first month of the quarter to which scouts must pay quarter contributions.
     */
    const CONTRIBUTIONS_LAST_CHARGE_DAY = "CONTRIBUTIONS_LAST_CHARGE_DAY";

    /**
     * Returns associative settings array of key => value.
     *
     * @return array
     */
    public static function getSettingsArray()
    {
        return Setting::all()
            ->pluck("value", "name")
            ->toArray();
    }

    /**
     * Returns setting value from database.
     *
     * @param string $key
     * @return string|null
     * @throws SettingNotFoundException
     */
    public static function getValue(string $key)
    {
        if (!isset($key) || is_null($key)) {
            return null;
        }

        $setting = Setting::whereKey($key)->first();

        if (!$setting) {
            throw new SettingNotFoundException();
        }

        return $setting->value;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name", "value"
    ];
}
