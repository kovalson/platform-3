<?php
declare(strict_types=1);
namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\UserUnit
 *
 * @method static Builder|UserUnit newModelQuery()
 * @method static Builder|UserUnit newQuery()
 * @method static Builder|UserUnit query()
 * @mixin Eloquent
 * @property int $id
 * @property int $user_id
 * @property int $unit_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|UserUnit whereCreatedAt($value)
 * @method static Builder|UserUnit whereId($value)
 * @method static Builder|UserUnit whereUnitId($value)
 * @method static Builder|UserUnit whereUpdatedAt($value)
 * @method static Builder|UserUnit whereUserId($value)
 */
class UserUnit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "user_id",
        "unit_id"
    ];
}
