<?php
declare(strict_types=1);
namespace App\Models;

use App\Classes\ContributionsProfile;
use App\Http\Helpers\RequestKey;
use App\Traits\Filterable;
use App\Traits\Scopeable;
use App\Traits\Searchable;
use App\Traits\Sortable;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Scout
 *
 * @property-read District $district
 * @property-read Unit $unit
 * @method static Builder|Scout newModelQuery()
 * @method static Builder|Scout newQuery()
 * @method static Builder|Scout query()
 * @method static Builder|Scout searched($columns = [], $searchPhrase = '')
 * @method static Builder|Scout searchedByRegistrationNumber($searchPhrase = '')
 * @method static Builder|Scout sorted($sortColumns = [])
 * @mixin Eloquent
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $registration_number
 * @property int|null $unit_id
 * @property int|null $district_id
 * @property string|null $remarks
 * @property string|null $joined_at
 * @property string|null $quit_at
 * @property int $archived
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Scout whereArchived($value)
 * @method static Builder|Scout whereCreatedAt($value)
 * @method static Builder|Scout whereDistrictId($value)
 * @method static Builder|Scout whereFirstName($value)
 * @method static Builder|Scout whereId($value)
 * @method static Builder|Scout whereJoinedAt($value)
 * @method static Builder|Scout whereLastName($value)
 * @method static Builder|Scout whereQuitAt($value)
 * @method static Builder|Scout whereRegistrationNumber($value)
 * @method static Builder|Scout whereRemarks($value)
 * @method static Builder|Scout whereUnitId($value)
 * @method static Builder|Scout whereUpdatedAt($value)
 * @property-read string $display_name
 * @method static Builder|Scout commonDistricts()
 * @method static Builder|Scout commonRoles()
 * @method static Builder|Scout commonUnits()
 * @method static Builder|Scout referencingDistrict($district_id, $referenceKey = 'district_id')
 * @method static Builder|Scout referencingUnit($unit_id, $referenceKey = 'unit_id')
 * @method static Builder|Scout relatedToDistricts($districts = [], $relationship = 'districts')
 * @method static Builder|Scout relatedToUnits($units = [], $relationship = 'units')
 * @method static Builder|Scout scoped($permission = null)
 * @method static Builder|Scout withIds($ids = [], $table = '')
 * @method static Builder|Scout withRoles($roles = [])
 * @property-read Collection|Contribution[] $contributions
 * @property-read int|null $contributions_count
 * @property-read array $contributions_profile
 * @method static Builder|Scout filtered($filterColumns = [])
 * @method static Builder|Scout searchedWithColumns($columns, $searchPhrase = '')
 * @method static Builder|Scout filteredWithColumns($columns)
 */
class Scout extends Model
{
    use Filterable;
    use Searchable;
    use Sortable;
    use Scopeable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "first_name", "last_name",
        "registration_number",
        "unit_id", "district_id",
        "remarks",
        "joined_at", "quit_at",
        "archived"
    ];

    /**
     * The columns to search when filtering model results.
     *
     * @var string[]
     */
    protected $searchColumns = [
        "first_name",
        "last_name",
        "unit.name",
        "district.name",
        "remarks",
        "joined_at",
        "quit_at"
    ];

    /**
     * The columns to sort the results by by default.
     *
     * @var string[]
     */
    protected $sortColumns = [
        "last_name",
        "first_name"
    ];

    /**
     * The columns to filter the results by.
     *
     * @var string[]
     */
    protected $filterColumns = [
        "district_id",
        "unit_id"
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        "display_name",
        "contributions_profile"
    ];

    /**
     * Returns scout full contributions profile.
     *
     * @return array
     */
    public function getContributionsProfileAttribute(): array
    {
        $profile = new ContributionsProfile($this);

        return $profile->toArray();
    }

    /**
     * Returns full display name as an attribute.
     *
     * @return string
     */
    public function getDisplayNameAttribute(): string
    {
        return ($this->last_name . " " . $this->first_name);
    }

    /**
     * Returns relationship with unit it belongs to.
     *
     * @return BelongsTo
     */
    public function unit(): BelongsTo
    {
        return $this->belongsTo(Unit::class);
    }

    /**
     * Returns relationship with unit it belongs to.
     *
     * @return BelongsTo
     */
    public function units(): BelongsTo
    {
        return $this->unit();
    }

    /**
     * Returns relationship with district it belongs to.
     *
     * @return BelongsTo
     */
    public function district(): BelongsTo
    {
        return $this->belongsTo(District::class);
    }

    /**
     * Returns relationship with district it belongs to.
     *
     * @return BelongsTo
     */
    public function districts(): BelongsTo
    {
        return $this->district();
    }

    /**
     * Returns relationship with contributions it has.
     *
     * @return HasMany
     */
    public function contributions(): HasMany
    {
        return $this->hasMany(Contribution::class);
    }

    /**
     * Returns scout's district (if exists).
     *
     * @return District|null
     */
    public function getDistrict(): ?District
    {
        if ($this->district) {
            return $this->district;
        } else if ($this->unit) {
            return $this->unit->district;
        } else {
            return null;
        }
    }

    /**
     * Returns scout's contributions status array.
     *
     * @return array
     */
    public function getContributionsStatus(): array
    {
        $district = $this->getDistrict();
        $profile = $this->contributions_profile;

        // We remove the "scout_id" to hide sensitive information
        // about the scout
        unset($profile["scout_id"]);

        return [
            "amounts" => ContributionAmount::getContributionsAmountsArray(),
            "profile" => $profile,
            "remarks" => $this->remarks,
            "last_modified" => $district ? $district->last_modified : null,
            "transfer_details" => $district ? $district->transfer_details : null
        ];
    }

    /**
     * Checks whether the scout has paid contribution for given year and quarter.
     *
     * @param  int  $year
     * @param  int  $quarter
     * @return bool
     */
    public function hasPaidContributionFor(int $year, int $quarter): bool
    {
        return $this->contributions()
            ->where("year", $year)
            ->where("quarter" . $quarter, true)
            ->exists();
    }

    /**
     * Scopes the results by search phrase appended to the registration number.
     *
     * @param  Builder  $query
     * @param  string  $searchPhrase
     * @return Builder
     */
    public function scopeSearchedByRegistrationNumber(Builder $query, string $searchPhrase = ""): Builder
    {
        // We get the authorized user
        $user = auth()->user();

        // First we get the search phrase passed as argument and if it is empty
        // we try to take it from the request object
        if (trim($searchPhrase) === "") {
            $searchPhrase = request()->input(RequestKey::SEARCH_QUERY, "");
        }

        // We append the search phrase
        if ($user->can(config("permissions.scout.registration_number.view"))) {
            $query->orWhere("scouts.registration_number", "LIKE", "%$searchPhrase%");
        }

        return $query;
    }
}
