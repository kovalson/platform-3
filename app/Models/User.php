<?php
declare(strict_types=1);
namespace App\Models;

use App\Exceptions\UnauthorizedResourceActionException;
use App\Traits\Scopeable;
use App\Traits\Searchable;
use App\Traits\Sortable;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Laravel\Passport\Client;
use Laravel\Passport\HasApiTokens;
use Laravel\Passport\Token;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $last_logged_in
 * @property int $active
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereActive($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereLastLoggedIn($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Collection|Client[] $clients
 * @property-read int|null $clients_count
 * @property-read Collection|Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read Collection|Role[] $roles
 * @property-read int|null $roles_count
 * @property-read Collection|Token[] $tokens
 * @property-read int|null $tokens_count
 * @method static Builder|User permission($permissions)
 * @method static Builder|User role($roles, $guard = null)
 * @property-read Collection|District[] $districts
 * @property-read int|null $districts_count
 * @property-read Collection|Unit[] $units
 * @property-read int|null $units_count
 * @method static Builder|User searched($columns = [], $searchPhrase = '')
 * @method static Builder|User sorted($sortColumns = [])
 * @method static Builder|User commonDistricts()
 * @method static Builder|User commonRoles()
 * @method static Builder|User commonUnits()
 * @method static Builder|User referencingDistrict($district_id, $referenceKey = 'district_id')
 * @method static Builder|User referencingUnit($unit_id, $referenceKey = 'unit_id')
 * @method static Builder|User relatedToDistricts($districts = [], $relationship = 'districts')
 * @method static Builder|User relatedToUnits($units = [], $relationship = 'units')
 * @method static Builder|User scoped($permission = null)
 * @method static Builder|User withIds($ids = [], $table = '')
 * @method static Builder|User withRoles($roles = [])
 * @method static Builder|User active()
 * @method static Builder|User searchedWithColumns($columns, $searchPhrase = '')
 * @method static Builder|User others()
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use HasApiTokens;
    use Searchable;
    use Sortable;
    use Scopeable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name",
        "email", "password",
        "last_logged_in",
        "active"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        "password", "remember_token", "pivot"
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        "email_verified_at" => "datetime"
    ];

    /**
     * The columns to search the users by.
     *
     * @type string[]
     */
    protected $searchColumns = [
        "name",
        "email"
    ];

    /**
     * The columns to sort the users by by default.
     *
     * @type string[]
     */
    protected $sortColumns = [
        "name"
    ];

    /**
     * Returns default guard name for app.
     *
     * @return string
     */
    protected function getDefaultGuardName(): string
    {
        return "api";
    }

    /**
     * Returns districts related to the user.
     *
     * @return BelongsToMany
     */
    public function districts(): BelongsToMany
    {
        return $this->belongsToMany(District::class, "user_districts");
    }

    /**
     * Returns units related to the user.
     *
     * @return BelongsToMany
     */
    public function units(): BelongsToMany
    {
        return $this->belongsToMany(Unit::class, "user_units");
    }

    /**
     * Scopes the query to get only the active users.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where("active", true);
    }

    /**
     * Scopes users to other than oneself.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeOthers(Builder $query): Builder
    {
        return $query->where("id", "<>", auth()->user()->id);
    }

    /**
     * Returns array of user's roles names.
     *
     * @return array
     */
    public function getRolesArray(): array
    {
        return $this->getRoleNames()->toArray();
    }

    /**
     * Returns array of user's roles ids.
     *
     * @return array
     */
    public function getRolesIdsArray(): array
    {
        return $this->roles
            ->pluck("id")
            ->toArray();
    }

    /**
     * Returns array of user's permissions names.
     *
     * @return array
     */
    public function getPermissionsArray(): array
    {
        return $this->getAllPermissions()
            ->pluck("name")
            ->toArray();
    }

    /**
     * Returns array of all user's permissions scopes.
     *
     * @return array
     */
    public function getPermissionsScopesArray(): array
    {
        $rolesIds = $this->getRolesIdsArray();

        return RolePermissionScope::whereIn("role_id", $rolesIds)
            ->get()
            ->map(function (RolePermissionScope $scope) {
                return [
                    "role" => $scope->role->name,
                    "permission" => $scope->permission->name,
                    "scope" => $scope->scope,
                    "values" => $scope->values
                ];
            })
            ->toArray();
    }

    /**
     * Checks whether the user is a super administrator.
     *
     * @return bool
     */
    public function isSuperAdministrator(): bool
    {
        return $this->hasRole(config("roles.super_administrator"));
    }

    /**
     * Checks whether the user can edit scout's contributions.
     *
     * @param int|Scout $scout The scout model object or id.
     * @return bool
     */
    public function canEditScoutContributions($scout): bool
    {
        if ($scout instanceof Scout) {
            $scout = $scout->id;
        }

        return Scout::scoped(config("permissions.scout.contribution.edit"))
            ->where("id", $scout)
            ->exists();
    }

    /**
     * Synchronizes user's relationships with roles, districts and units using
     * given arrays of ids.
     *
     * @param  array|null  $rolesIds
     * @param  array|null  $districtsIds
     * @param  array|null  $unitsIds
     * @throws UnauthorizedResourceActionException
     */
    public function syncRelationships(array $rolesIds = null, array $districtsIds = null, array $unitsIds = null): void
    {
        // We check that the user creating the user has rights to assign
        // him the roles he chose. Should any role be illegal, an exception
        // will be thrown
        if (!is_null($rolesIds)) {
            Role::findMany($rolesIds)->each(function (Role $role) {
                $role->checkResourceActionAccess(config("permissions.user.assign.role"));
            });

            // We synchronize new roles for user
            $this->roles()->sync($rolesIds);
        }

        // We check that the user creating the user has rights to assign
        // him the districts he chose. Should any district be illegal, an exception
        // will be thrown
        if (!is_null($districtsIds)) {
            District::findMany($districtsIds)->each(function (District $district) {
                $district->checkResourceActionAccess(config("permissions.user.assign.district"));
            });

            // We synchronize new districts for user
            $this->districts()->sync($districtsIds);
        }

        // We check that the user creating the user has rights to assign
        // him the units he chose. Should any unit be illegal, an exception
        // will be thrown
        if (!is_null($unitsIds)) {
            Unit::findMany($unitsIds)->each(function (Unit $unit) {
                $unit->checkResourceActionAccess(config("permissions.user.assign.unit"));
            });

            // We synchronize new units for user
            $this->units()->sync($unitsIds);
        }
    }
}
