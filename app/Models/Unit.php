<?php
declare(strict_types=1);
namespace App\Models;

use App\Exceptions\CannotDeleteUnitException;
use App\Traits\Filterable;
use App\Traits\Scopeable;
use App\Traits\Searchable;
use App\Traits\Sortable;
use Eloquent;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Unit
 *
 * @property int $id
 * @property string $name
 * @property int|null $district_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read District|null $district
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|Unit newModelQuery()
 * @method static Builder|Unit newQuery()
 * @method static Builder|Unit query()
 * @method static Builder|Unit searched($columns = [], $searchPhrase = '')
 * @method static Builder|Unit sorted($sortColumns = [])
 * @method static Builder|Unit whereCreatedAt($value)
 * @method static Builder|Unit whereDistrictId($value)
 * @method static Builder|Unit whereId($value)
 * @method static Builder|Unit whereName($value)
 * @method static Builder|Unit whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Collection|Scout[] $scouts
 * @property-read int|null $scouts_count
 * @method static Builder|Unit commonDistricts()
 * @method static Builder|Unit commonRoles()
 * @method static Builder|Unit commonUnits()
 * @method static Builder|Unit referencingDistrict($district_id, $referenceKey = 'district_id')
 * @method static Builder|Unit referencingUnit($unit_id, $referenceKey = 'unit_id')
 * @method static Builder|Unit relatedToDistricts($districts = [], $relationship = 'districts')
 * @method static Builder|Unit relatedToUnits($units = [], $relationship = 'units')
 * @method static Builder|Unit scoped($permission = null)
 * @method static Builder|Unit withIds($ids = [], $table = '')
 * @method static Builder|Unit withRoles($roles = [])
 * @method static Builder|Unit filtered($filterColumns = [])
 * @method static Builder|Unit searchedWithColumns($columns, $searchPhrase = '')
 * @method static Builder|Unit filteredWithColumns($columns)
 */
class Unit extends Model
{
    use Searchable;
    use Sortable;
    use Scopeable;
    use Filterable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name",
        "district_id"
    ];

    /**
     * The columns to search when filtering model results.
     *
     * @var string[]
     */
    private $searchColumns = [
        "name",
        "district.name"
    ];

    /**
     * The columns to sort the results by by default.
     *
     * @var string[]
     */
    private $sortColumns = [
        "name"
    ];

    /**
     * The columns to filter the results by.
     *
     * @var string[]
     */
    private $filterColumns = [
        "district_id"
    ];

    /**
     * Returns district the unit belongs to.
     *
     * @return BelongsTo
     */
    public function district(): BelongsTo
    {
        return $this->belongsTo(District::class, "district_id");
    }

    /**
     * Returns district the unit belongs to.
     *
     * @return BelongsTo
     */
    public function districts(): BelongsTo
    {
        return $this->district();
    }

    /**
     * Returns all scouts that belong to the unit.
     *
     * @return HasMany
     */
    public function scouts(): HasMany
    {
        return $this->hasMany(Scout::class, "unit_id");
    }

    /**
     * Returns all users related to the unit.
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            "user_units",
            "unit_id",
            "user_id"
        );
    }

    /**
     * Delete the model from the database.
     *
     * @return bool|null
     * @throws CannotDeleteUnitException|Exception
     */
    public function delete()
    {
        // We check if the unit has any scouts
        // If so - the unit cannot be deleted
        if ($this->scouts()->exists()) {
            throw CannotDeleteUnitException::hasScouts();
        }

        // If the unit has no scouts assigned
        // we delete it by running the parent delete() method
        return parent::delete();
    }
}
