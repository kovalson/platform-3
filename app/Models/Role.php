<?php
declare(strict_types=1);
namespace App\Models;

use App\Exceptions\CannotDeleteRoleException;
use App\Exceptions\CannotUpdateRoleException;
use App\Traits\Scopeable;
use App\Traits\Searchable;
use App\Traits\Sortable;
use Eloquent;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role as SpatieRole;

/**
 * App\Models\Role
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|Role newModelQuery()
 * @method static Builder|Role newQuery()
 * @method static Builder|SpatieRole permission($permissions)
 * @method static Builder|Role query()
 * @method static Builder|Role searched($columns = [], $searchPhrase = '')
 * @method static Builder|Role sorted($sortColumns = [])
 * @method static Builder|Role whereCreatedAt($value)
 * @method static Builder|Role whereGuardName($value)
 * @method static Builder|Role whereId($value)
 * @method static Builder|Role whereName($value)
 * @method static Builder|Role whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Collection|RolePermissionScope[] $permissionsScopes
 * @property-read int|null $permissions_scopes_count
 * @method static Builder|Role commonDistricts()
 * @method static Builder|Role commonRoles()
 * @method static Builder|Role commonUnits()
 * @method static Builder|Role referencingDistrict($district_id = 0, $referenceKey = 'district_id')
 * @method static Builder|Role referencingUnit($unit_id = 0, $referenceKey = 'unit_id')
 * @method static Builder|Role relatedToDistricts($districts = [], $relationship = 'districts')
 * @method static Builder|Role relatedToUnits($units = [], $relationship = 'units')
 * @method static Builder|Role scoped($permission = null)
 * @method static Builder|Role withIds($ids = [], $table = '')
 * @method static Builder|Role withRoles($roles = [])
 * @method static Builder|Role searchedWithColumns($columns, $searchPhrase = '')
 */
class Role extends SpatieRole
{
    use Searchable, Sortable, Scopeable;

    /**
     * The columns to search the roles by.
     *
     * @type string[]
     */
    protected $searchColumns = [
        "name"
    ];

    /**
     * The columns to sort the roles by by default.
     *
     * @type string[]
     */
    protected $sortColumns = [
        "name"
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        "guard_name"
    ];

    /**
     * Returns relationship with permission scopes it has.
     *
     * @return HasMany
     */
    public function permissionsScopes(): HasMany
    {
        return $this->hasMany(RolePermissionScope::class);
    }

    /**
     * Checks whether the role is a "super administrator" role.
     *
     * @return bool
     */
    public function isSuperAdmin()
    {
        return ($this->name === config("roles.super_administrator"));
    }

    /**
     * Update the model in the database.
     *
     * @param  array  $attributes
     * @param  array  $options
     * @return bool
     * @throws CannotUpdateRoleException
     */
    public function update(array $attributes = [], array $options = [])
    {
        // We need to check if the role is a "super administrator" role.
        // If so - the role cannot be updated.
        if ($this->isSuperAdmin()) {
            throw new CannotUpdateRoleException();
        }

        return parent::update($attributes, $options);
    }

    /**
     * Delete the model from the database.
     *
     * @return bool|null
     * @throws CannotDeleteRoleException|Exception
     */
    public function delete()
    {
        // We need to check if the role is a "super administrator" role.
        // If so - the role cannot be deleted in any way by a user
        if ($this->isSuperAdmin()) {
            throw new CannotDeleteRoleException();
        }

        // If the role is not a "super administrator" role
        // we delete it by running the parent delete() method
        return parent::delete();
    }
}
