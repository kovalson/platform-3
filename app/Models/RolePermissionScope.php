<?php
declare(strict_types=1);
namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Spatie\Permission\Models\Permission;

/**
 * App\Models\RolePermissionScope
 *
 * @method static Builder|RolePermissionScope newModelQuery()
 * @method static Builder|RolePermissionScope newQuery()
 * @method static Builder|RolePermissionScope query()
 * @mixin Eloquent
 * @property int $id
 * @property int $role_id
 * @property int $permission_id
 * @property string $scope
 * @property string $values
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|RolePermissionScope whereCreatedAt($value)
 * @method static Builder|RolePermissionScope whereId($value)
 * @method static Builder|RolePermissionScope wherePermissionId($value)
 * @method static Builder|RolePermissionScope whereRoleId($value)
 * @method static Builder|RolePermissionScope whereScope($value)
 * @method static Builder|RolePermissionScope whereUpdatedAt($value)
 * @method static Builder|RolePermissionScope whereValues($value)
 * @property-read Permission $permission
 * @property-read Role $role
 */
class RolePermissionScope extends Model
{
    /**
     * Returns all scopes for given permission that user has.
     * Method gets the authenticated user.
     *
     * @param  string  $permissionName
     * @return Collection
     */
    public static function findForUserPermission(string $permissionName): Collection
    {
        // We first retrieve the permission model
        $permission = Permission::findByName($permissionName);

        // Then we retrieve current user's roles
        $roles = auth()->user()->getRolesIdsArray();

        // We return all matching scopes
        return RolePermissionScope::whereIn("role_id", $roles)
            ->where("permission_id", $permission->id)
            ->get();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "role_id",
        "permission_id",
        "scope",
        "values"
    ];

    /**
     * Returns relationship with the role it belongs to.
     *
     * @return BelongsTo
     */
    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * Returns relationship with the permission it belongs to.
     *
     * @return BelongsTo
     */
    public function permission(): BelongsTo
    {
        return $this->belongsTo(Permission::class);
    }

    /**
     * Converts scope values to array of values.
     *
     * @param  bool  $castToInteger
     * @return array
     */
    public function getValuesArray($castToInteger = true): array
    {
        // We convert values to array
        $values = explode(",", $this->values);

        // We cast values to integers if necessary
        if ($castToInteger) {
            return array_map(function (string $value) {
                return (int) $value;
            }, $values);
        }

        return $values;
    }
}
