<?php
declare(strict_types=1);
namespace App\Rules;

use Arr;
use Illuminate\Contracts\Validation\Rule;

class RolePermissionScope implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $rolePermissionScopes = Arr::flatten(config("role_permission_scopes"));

        return in_array($value, $rolePermissionScopes);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __("Podany filtr (:value) nie został zidentyfikowany.");
    }
}
