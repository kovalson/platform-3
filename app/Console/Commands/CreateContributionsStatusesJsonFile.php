<?php
declare(strict_types=1);
namespace App\Console\Commands;

use App\Classes\ContributionStatus;
use Illuminate\Console\Command;
use ReflectionClass;

class CreateContributionsStatusesJsonFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "command:createContributionsStatusesJsonFile";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Creates JSON file for contributions statuses based on the ContributionStatus class.";

    /**
     * Output file name for permissions categories.
     *
     * @type string
     */
    const OUTPUT_FILE = "json/contributions_statuses.json";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return bool
     */
    public function handle()
    {
        // We get the contributions statuses array
        $array = $this->getContributionsStatusesArray();

        // We create the output file
        $this->createOutputFile($array);

        return true;
    }

    /**
     * Returns array of all possible contributions statuses.
     *
     * @return array
     */
    private function getContributionsStatusesArray(): array
    {
        // We get the class constants using reflection
        $reflection = new ReflectionClass(ContributionStatus::class);

        return $reflection->getConstants();
    }

    /**
     * Creates output JSON file for given array of contributions statuses.
     *
     * @param  array  $contributionsStatuses
     * @return void
     */
    private function createOutputFile(array $contributionsStatuses): void
    {
        $path = resource_path(self::OUTPUT_FILE);
        $json = json_encode($contributionsStatuses, JSON_PRETTY_PRINT);
        $contents = mb_convert_case($json, MB_CASE_LOWER, "UTF-8");

        file_put_contents($path, $contents);
    }
}
