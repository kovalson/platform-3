<?php
declare(strict_types=1);
namespace App\Console\Commands;

use Error;
use Illuminate\Console\Command;

class VueComponent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "vue:component {component}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Creates Vue component file basing on given configuration.";

    /**
     * The name of the created Vue component.
     *
     * @type string
     */
    private $componentName;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return bool
     * @throws Error
     */
    public function handle()
    {
        // We acquire the component name from the given argument
        $this->componentName = $this->argument("component");

        // We assure that the component name exists
        if (is_null($this->componentName) || empty($this->componentName) || !$this->componentName) {
            throw new Error("Component name was not provided!");
        }

        $savedBytes = file_put_contents(
            resource_path("js/" . $this->componentName . ".vue"),
            $this->getComponentFileContents()
        );

        if ($savedBytes) {
            $this->info("Component " . basename($this->componentName) . " was successfully created!");
        }

        return !!$savedBytes;
    }

    /**
     * Returns Vue component file contents to save.
     *
     * @return string
     */
    private function getComponentFileContents()
    {
        // We extract the component name from the path
        $componentName = basename($this->componentName);

        return "<template>
    <div>
        <h1>
            ${componentName}
        </h1>
    </div>
</template>

<script lang=\"ts\">
    import Vue from \"vue\";
    import Component from \"vue-class-component\";

    @Component
    export default class ${componentName} extends Vue {}
</script>
";
    }
}
