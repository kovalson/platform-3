<?php
declare(strict_types=1);
namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class ConvertRoles extends Command
{
    /**
     * Returns permissions names mapping (from old permissions names to new names).
     *
     * @return array
     */
    private static function getPermissionsMapping(): array
    {
        return [
            "edit last modified date" => config("permissions.district.last_modified.edit"),
            "transfer details settings" => config("permissions.district.transfer_details.edit"),
            "view archived scouts" => config("permissions.scout.archived.access"),
            "view registration number" => config("permissions.scout.registration_number.display"),
            "edit registration number" => config("permissions.scout.registration_number.edit"),
            "update scout contributions" => config("permissions.scout.contribution.edit"),
            "contributions settings" => config("permissions.setting.contributions.edit"),
            "amounts settings" => config("permissions.setting.contributions_amounts.edit")
        ];
    }

    /**
     * Returns permissions names to be removed from the database.
     *
     * @return string[]
     */
    private static function getPermissionsToRemove(): array
    {
        return [
            "autocomplete units",
            "view user",
            "view districts",
            "view district administrators",
            "create district administrator",
            "edit district administrator",
            "delete district administrator",
            "view units",
            "view unit administrators",
            "create unit administrator",
            "edit unit administrator",
            "delete unit administrator",
            "view scouts",
            "view roles"
        ];
    }

    /**
     * Returns new permissions to be inserted into the database.
     *
     * @return string[]
     */
    private static function getPermissionsToInsert(): array
    {
        return [
            config("permissions.user.assign.role"),
            config("permissions.user.assign.district"),
            config("permissions.user.assign.unit"),
            config("permissions.unit.assign.district"),
            config("permissions.scout.assign.unit"),
            config("permissions.scout.assign.district"),
            config("permissions.scout.archived.restore"),
            config("permissions.scout.delete"),
            config("permissions.setting.contributions.access"),
            config("permissions.setting.contributions_amounts.access")
        ];
    }

    /**
     * Returns old permissions reassignments to new permissions.
     *
     * @return array
     */
    private static function getPermissionsReassignments(): array
    {
        return [
            "view roles" => config("permissions.role.access"),
            "view user" => config("permissions.user.access"),
            "view districts" => config("permissions.district.access"),
            "view units" => config("permissions.unit.access"),
            "view scouts" => config("permissions.scout.access")
        ];
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "command:convertRoles";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Converts existing roles from the remote database to new roles.";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return boolean
     * @throws Exception
     */
    public function handle(): bool
    {
        // We create initial summary for the database
        $this->createInitialSummary();

        // First we give existing permissions new names
        $this->renameExistingPermissions();

        // Then we add new permissions
        $this->addNewPermissions();

        // Then we reassign roles with new permissions
        $this->reassignRolesWithPermissions();

        // Then we remove unnecessary permissions
        $this->removeUnnecessaryPermissions();

        // We create final summary for the database
        $this->createFinalSummary();

        // We return true if command execution was successful
        return true;
    }

    /**
     * Creates initial summary for the database and logs it.
     *
     * @return void
     */
    private function createInitialSummary(): void
    {
        // We get permissions data from the database
        $permissions = Permission::all();
        $permissionsCount = $permissions->count();

        // Log the information
        $this->info("There are " . $permissionsCount . " permissions in total.");
        $this->info("These are as follows: " . json_encode($permissions->pluck("name")->join(", ")));
    }

    /**
     * Creates final summary for the database after executing the command and logs it.
     *
     * @return void
     */
    private function createFinalSummary(): void
    {
        // We get permissions data from the database
        $permissions = Permission::all();
        $permissionsCount = $permissions->count();

        // Log the information
        $this->info("There are " . $permissionsCount . " permissions in total.");
        $this->info("These are as follows: " . json_encode($permissions->pluck("name")->join(", ")));
    }

    /**
     * Renames existing permissions.
     *
     * @return void
     */
    private function renameExistingPermissions(): void
    {
        // We get the permissions names mapping
        $permissionsMapping = self::getPermissionsMapping();

        // We loop through all permissions from the mapping
        foreach ($permissionsMapping as $oldName => $newName) {

            // Get the permission from the database
            $permission = Permission::where("name", $oldName)->first();

            // If there exists such permission - we rename it
            if ($permission) {
                $permission->update(["name" => $newName]);

                // We also log the information
                $this->info("Permission '" . $oldName . "' was renamed to '" . $newName . "'.");
            } else {

                // We just log the information that the permission was not found
                $this->warn("Permission '" . $oldName . "' was not found in the database!");

            }

        }
    }

    /**
     * Inserts new permissions into the database.
     *
     * @return void
     */
    private function addNewPermissions(): void
    {
        // We get new permissions
        $permissions = self::getPermissionsToInsert();

        // We loop through all permissions
        foreach ($permissions as $permission) {

            // We insert new permission into the database
            Permission::updateOrCreate([
                "name" => $permission,
                "guard_name" => "api"
            ]);

        }

        // We log the information
        $this->info(count($permissions) . " new permissions were added.");
    }

    /**
     * Reassigns roles with new permissions and corrects the existing ones.
     *
     * @return void
     */
    private function reassignRolesWithPermissions(): void
    {
        // We get all roles from the database
        $roles = Role::all();

        // We get all reassignments
        $reassignments = self::getPermissionsReassignments();

        // We loop through each role
        foreach ($roles as $role) {

            // We omit the super administrator
            if ($role->name === config("roles.super_administrator")) {
                continue;
            }

            // We loop through each reassignment and apply id
            foreach ($reassignments as $oldPermission => $newPermission) {

                // Proceed only if the role really has the old permission
                if ($role->hasPermissionTo($oldPermission)) {

                    // Assign new permission to the role only if it does not have it yet
                    if (!$role->hasPermissionTo($newPermission)) {
                        $role->givePermissionTo($newPermission);

                        // We also log the information
                        $this->info("Role's' " . $role->name . " old permission '" . $oldPermission .
                            "' was reassigned to new permission '" . $newPermission . "'.");
                    }

                }

            }

        }
    }

    /**
     * Removes unnecessary permissions.
     *
     * @return void
     */
    private function removeUnnecessaryPermissions(): void
    {
        // We get the unnecessary permissions
        $permissionsToRemove = self::getPermissionsToRemove();

        // We loop through each permission and delete it
        foreach ($permissionsToRemove as $permissionName) {

            // We get permission from the database
            $permission = Permission::where("name", $permissionName)->first();

            // We try to delete the permission
            try {
                $permission->delete();

                // We also log the information
                $this->info("Permission '" . $permissionName . "' was deleted.");
            } catch (Exception $exception) {
                $this->error("Permission '" . $permissionName . "' could not be deleted.");
            }

        }
    }
}
