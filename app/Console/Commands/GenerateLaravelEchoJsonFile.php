<?php
declare(strict_types=1);
namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateLaravelEchoJsonFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "command:generateLaravelEchoJsonFile";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generates a laravel_echo.json file based on laravel_echo.php config.";

    /**
     * Output file name for permissions categories.
     *
     * @type string
     */
    const OUTPUT_FILE = "json/laravel_echo.json";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        // We get the config array
        $configArray = config("laravel_echo");

        // We create the output file
        $this->createOutputFile($configArray);
    }

    /**
     * Creates output JSON file with given content.
     *
     * @param  array  $content
     * @return void
     */
    private function createOutputFile(array $content): void
    {
        $path = resource_path(self::OUTPUT_FILE);
        $json = json_encode($content, JSON_PRETTY_PRINT);
        $contents = mb_convert_case($json, MB_CASE_LOWER, "UTF-8");

        file_put_contents($path, $contents);
    }
}
