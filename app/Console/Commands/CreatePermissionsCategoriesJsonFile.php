<?php
declare(strict_types=1);
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class CreatePermissionsCategoriesJsonFile extends Command
{
    /**
     * Permissions file name.
     *
     * @type string
     */
    const PERMISSIONS_FILE = "json/permissions.json";

    /**
     * Output file name for permissions categories.
     *
     * @type string
     */
    const OUTPUT_FILE = "json/permissions_categories.json";

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "command:createPermissionsCategoriesJsonFile";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Creates JSON file with permissions categories based on permissions.json.";

    /**
     * Permissions source file.
     *
     * @var string
     */
    private $sourceFile;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->sourceFile = self::PERMISSIONS_FILE;
    }

    /**
     * Execute the console command.
     *
     * @return bool
     */
    public function handle(): bool
    {
        // First try to get file contents
        $fileContents = file_get_contents(resource_path($this->sourceFile));

        // We convert the contents into an array
        $permissionsArray = json_decode($fileContents, true);

        // We create the permissions to categories map array
        $permissionsCategories = $this->mapPermissionsToCategories($permissionsArray);

        // Finally we write the results into output file
        $this->createOutputFile($permissionsCategories);

        return true;
    }

    /**
     * Creates new array with permissions mapped to categories.
     *
     * @param  array  $permissionsArray
     * @return array
     */
    private function mapPermissionsToCategories(array $permissionsArray): array
    {
        $mapped = [];

        foreach ($permissionsArray as $category => $subGroup) {
            $this->mapLeavesToCategory($subGroup, $category, $mapped);
        }

        return $mapped;
    }

    /**
     * Maps all permissions from given subgroup into given category in given array.
     *
     * @param  array  $subGroup
     * @param  string  $category
     * @param  array  $mapped
     * @return void
     */
    private function mapLeavesToCategory(array $subGroup, string $category, array &$mapped): void
    {
        $flatArray = Arr::flatten($subGroup);

        foreach ($flatArray as $permission) {
            $mapped[$permission] = $category;
        }
    }

    /**
     * Encodes given permissions categories array and saves it
     * into the output file.
     *
     * @param $permissionCategories
     * @return void
     */
    private function createOutputFile($permissionCategories): void
    {
        $path = resource_path(self::OUTPUT_FILE);
        $contents = json_encode($permissionCategories, JSON_PRETTY_PRINT);

        file_put_contents($path, $contents);
    }
}
