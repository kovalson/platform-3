<?php
declare(strict_types=1);
namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreatePermissionsConfig extends Command
{
    /**
     * Name of default source file.
     */
    const DEFAULT_SOURCE_FILE = "json/permissions.json";

    /**
     * Name of default output file.
     */
    const DEFAULT_OUTPUT_FILE = "permissions.php";

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "command:createPermissionsConfig";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Creates permissions config file from permissions.json.";

    /**
     * Source JSON file to transform.
     *
     * @var string
     */
    private $sourceFile;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->sourceFile = self::DEFAULT_SOURCE_FILE;
    }

    /**
     * Execute the console command.
     *
     * @return boolean
     */
    public function handle()
    {
        // First try to get file contents
        $fileContents = file_get_contents(resource_path($this->sourceFile));

        // Transform contents
        $transformed = $this->transformContents($fileContents);

        // Save to file
        if ($this->saveToFile($transformed)) {
            echo "Successfully saved file.";
        }

        return true;
    }

    /**
     * Transforms file contents into PHP file with array.
     *
     * @param string $contents
     * @return string
     */
    private function transformContents(string $contents)
    {
        $transformed  = "<?php\n";
        $transformed .= "
/**
 * Do NOT edit this file.
 * This file was automatically generated using \"createPermissionsConfig\" command. Use this command
 * in order to regenerate this file contents.
*/

return ";
        $transformed .= str_replace(["{", "}", ":"], ["[", "]", " =>"], $contents);
        $transformed  = substr($transformed, 0, -2) .";";
        $transformed .= "\n";

        return $transformed;
    }

    /**
     * Saves generated contents to output file.
     *
     * @param string $contents
     * @return boolean
     */
    private function saveToFile(string $contents)
    {
        return file_put_contents(
            config_path(self::DEFAULT_OUTPUT_FILE),
            $contents
        );
    }
}
