<?php
declare(strict_types=1);
namespace App\Providers;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->createPaginatedMacro();
    }

    /**
     * Creates a "paginated" macro method for Response class.
     * The method is also callable by response() helper, for example:
     *  response()->paginated(...)
     *
     * @return void
     */
    private function createPaginatedMacro()
    {
        Response::macro("paginated", function (LengthAwarePaginator $paginator, string $dataKey) {
            // We first get the original items from the paginator
            $data = $paginator->items();

            $pagination = $paginator->toArray();

            // We remove the data key and its values from the array
            // not to unnecessarily return items twice
            unset($pagination["data"]);

            return Response::json([
                $dataKey => $data,
                "pagination" => $pagination
            ]);
        });
    }
}
