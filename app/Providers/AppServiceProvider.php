<?php
declare(strict_types=1);
namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Default string length for MySQL.
     *
     * @type int
     */
    const DEFAULT_STRING_LENGTH = 191;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Get string length
        $stringLength = config("database.connections.mysql.string_length", self::DEFAULT_STRING_LENGTH);

        // Set MySQL default string length
        Schema::defaultStringLength($stringLength);
    }
}
