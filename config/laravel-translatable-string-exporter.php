<?php

return [

    // Directories to search in
    "directories" => [
        "app",
        "resources"
    ],

    // File Patterns to search for
    "patterns" => [
        "*.js",
        "*.php",
        "*.ts",
        "*.vue"
    ],

    // Indicates whether new lines are allowed in translations.
    // It's best to keep it as "true" because otherwise new-lined texts
    // won't appear in the translations at all and will be harder to find
    "allow-newlines" => true,

    // Translation function names.
    // If your function name contains $ escape it using \$
    "functions" => [
        "__",
        "_t",
        "@lang",
        'this\.\$lang',
        "\$lang"
    ],

    // Indicates whether you need to sort the translations alphabetically by original strings (keys).
    // It helps navigate a translation file and detect possible duplicates
    "sort-keys" => true

];
