<?php

use App\Models\Unit;
use Illuminate\Database\Seeder;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We get the units from the remote database
        $units = DB::connection("mysql_remote")
            ->table("units")
            ->get();

        // We seed the database with new units
        foreach ($units as $unit) {
            Unit::updateOrCreate(["id" => $unit->id], [
                "id" => $unit->id,
                "name" => $unit->name,
                "district_id" => $unit->district_id,
                "created_at" => $unit->created_at,
                "updated_at" => $unit->updated_at
            ]);
        }
    }
}
