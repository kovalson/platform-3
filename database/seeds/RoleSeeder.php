<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We get the roles from the remote database
        $roles = DB::connection("mysql_remote")
            ->table("roles")
            ->get();

        // We seed the database with new roles
        foreach ($roles as $role) {
            Role::updateOrCreate(["id" => $role->id], [
                "id" => $role->id,
                "name" => $this->renameSuperAdministratorRole($role->name),
                "guard_name" => $role->guard_name,
                "created_at" => $role->created_at,
                "updated_at" => $role->updated_at
            ]);
        }
    }

    /**
     * Renames super administrator role.
     *
     * @param  string  $roleName
     * @return string
     */
    private function renameSuperAdministratorRole(string $roleName)
    {
        return $roleName === "super administrator"
            ? config("roles.super_administrator")
            : $roleName;
    }
}
