<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We get the settings from the remote database
        $settings = DB::connection("mysql_remote")
            ->table("settings")
            ->get();

        // We seed the database with new settings
        foreach ($settings as $setting) {
            Setting::updateOrCreate(["id" => $setting->id], [
                "id" => $setting->id,
                "name" => $setting->name,
                "value" => $setting->value,
                "created_at" => $setting->created_at,
                "updated_at" => $setting->updated_at
            ]);
        }
    }
}
