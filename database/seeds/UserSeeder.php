<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We get the users from the remote database
        $users = DB::connection("mysql_remote")
            ->table("users")
            ->get();

        // We seed the database with new users
        foreach ($users as $user) {
            User::updateOrCreate(["id" => $user->id], [
                "id" => $user->id,
                "name" => $user->name,
                "email" => $user->email,
                "email_verified_at" => $user->email_verified_at,
                "password" => $user->password,
                "last_logged_in" => $user->last_logged_in,
                "active" => 1,
                "remember_token" => $user->remember_token,
                "created_at" => $user->created_at,
                "updated_at" => $user->updated_at
            ]);
        }
    }
}
