<?php
declare(strict_types=1);

use Illuminate\Database\Seeder;

class RoleHasPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We get the data from the remote database
        $roleHasPermissions = DB::connection("mysql_remote")
            ->table("role_has_permissions")
            ->get();

        // We create an auxiliary array that will be inserted into the database
        $insertionArray = [];

        // We loop through all role-permissions relations
        foreach ($roleHasPermissions as $roleHasPermission) {
            $insertionArray[] = [
                "role_id" => $roleHasPermission->role_id,
                "permission_id" => $roleHasPermission->permission_id
            ];
        }

        // We seed the database with new data
        DB::connection("mysql")
            ->table("role_has_permissions")
            ->insertOrIgnore($insertionArray);
    }
}
