<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        // First we check connection with the remote database
        $this->checkRemoteConnection();

        // We seed the database with data from the remote database
        $this->call([
            UserSeeder::class,
            DistrictSeeder::class,
            UnitSeeder::class,
            UserDistrictSeeder::class,
            UserUnitSeeder::class,
            ScoutSeeder::class,
            ContributionSeeder::class,
            ContributionAmountSeeder::class,
            SettingSeeder::class,
            RoleSeeder::class,
            PermissionSeeder::class,
            RoleHasPermissionsSeeder::class,
            ModelHasRolesSeeder::class
        ]);

        // We call the command converting old permissions to new permissions
        Artisan::call("command:convertRoles");

        // We install passport clients in order to be able to use API requests safely
        Artisan::call("passport:install");
    }

    /**
     * Checks if connection with remote database can be established.
     *
     * @return void
     * @throws Error
     */
    private function checkRemoteConnection(): void
    {
        // We check whether we can establish connection
        if (!DB::connection("mysql_remote")) {
            throw new Error("Couldn't establish connection with remote database.");
        }

        // Then we try to get some data to check if that works
        try {
            DB::connection("mysql_remote")
                ->table("settings")
                ->get();
        } catch (Exception $exception) {
            throw new Error("Couldn't establish connection with remote database.");
        }
    }
}
