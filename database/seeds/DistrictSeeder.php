<?php

use App\Models\District;
use Illuminate\Database\Seeder;

class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We get the districts from the remote database
        $districts = DB::connection("mysql_remote")
            ->table("districts")
            ->get();

        // We seed the database with new districts
        foreach ($districts as $district) {
            District::updateOrCreate(["id" => $district->id], [
                "id" => $district->id,
                "name" => $district->name,
                "last_modified" => $district->last_modified,
                "transfer_details" => $district->transfer_details,
                "created_at" => $district->created_at,
                "updated_at" => $district->updated_at
            ]);
        }
    }
}
