<?php

use App\Models\ContributionAmount;
use Illuminate\Database\Seeder;

class ContributionAmountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We get the contributions amounts from the remote database
        $contributionsAmounts = DB::connection("mysql_remote")
            ->table("contributions_amounts")
            ->get();

        // We seed the database with new contributions amounts
        foreach ($contributionsAmounts as $amount) {
            ContributionAmount::updateOrCreate(["year" => $amount->year], [
                "year" => $amount->year,
                "quarter1" => $amount->quarter1,
                "quarter2" => $amount->quarter2,
                "quarter3" => $amount->quarter3,
                "quarter4" => $amount->quarter4,
                "created_at" => $amount->created_at,
                "updated_at" => $amount->updated_at
            ]);
        }
    }
}
