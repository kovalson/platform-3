<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We get the permissions from the remote database
        $permissions = DB::connection("mysql_remote")
            ->table("permissions")
            ->get();

        // We seed the database with new permissions
        foreach ($permissions as $permission) {
            Permission::updateOrCreate(["id" => $permission->id], [
                "id" => $permission->id,
                "name" => $permission->name,
                "guard_name" => "api",
                "created_at" => $permission->created_at,
                "updated_at" => $permission->updated_at
            ]);
        }
    }
}
