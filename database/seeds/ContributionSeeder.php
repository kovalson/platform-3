<?php

use App\Models\Contribution;
use Illuminate\Database\Seeder;

class ContributionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We get the contributions from the remote database
        $contributions = DB::connection("mysql_remote")
            ->table("contributions")
            ->get();

        // We seed the database with new contributions
        foreach ($contributions as $contribution) {
            Contribution::updateOrCreate(["id" => $contribution->id], [
                "id" => $contribution->id,
                "scout_id" => $contribution->scout_id,
                "year" => $contribution->year,
                "quarter1" => $contribution->quarter1,
                "quarter2" => $contribution->quarter2,
                "quarter3" => $contribution->quarter3,
                "quarter4" => $contribution->quarter4,
                "created_at" => $contribution->created_at,
                "updated_at" => $contribution->updated_at
            ]);
        }
    }
}
