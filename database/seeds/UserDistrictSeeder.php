<?php

use App\Models\UserDistrict;
use Illuminate\Database\Seeder;

class UserDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We get the user districts from the remote database
        $userDistricts = DB::connection("mysql_remote")
            ->table("user_districts")
            ->get();

        // We seed the database with new user districts
        foreach ($userDistricts as $userDistrict) {
            UserDistrict::updateOrCreate(["id" => $userDistrict->id], [
                "id" => $userDistrict->id,
                "user_id" => $userDistrict->user_id,
                "district_id" => $userDistrict->district_id,
                "created_at" => $userDistrict->created_at,
                "updated_at" => $userDistrict->updated_at
            ]);
        }
    }
}
