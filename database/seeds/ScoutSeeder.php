<?php

use App\Models\Scout;
use App\Models\Unit;
use Illuminate\Database\Seeder;

class ScoutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We get scouts from the remote database
        $scouts = DB::connection("mysql_remote")
            ->table("scouts")
            ->get();

        // We seed the database with new scouts
        foreach ($scouts as $scout) {

            // We set the default district as null for scout
            $district_id = null;

            // If scout belongs to a unit that belongs to a district
            // this scout also belongs to that district
            if ($scout->unit_id) {
                $unit = Unit::find($scout->unit_id);

                if ($unit->district_id) {
                    $district_id = $unit->district_id;
                }
            }

            Scout::updateOrCreate(["id" => $scout->id], [
                "id" => $scout->id,
                "first_name" => $scout->first_name,
                "last_name" => $scout->last_name,
                "registration_number" => $scout->registration_number,
                "unit_id" => $scout->unit_id,
                "district_id" => $district_id,
                "remarks" => $scout->remarks,
                "joined_at" => $scout->joined_at,
                "quit_at" => $scout->quit_at,
                "archived" => $scout->archived,
                "created_at" => $scout->created_at,
                "updated_at" => $scout->updated_at
            ]);
        }
    }
}
