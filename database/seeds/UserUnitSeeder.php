<?php

use App\Models\UserUnit;
use Illuminate\Database\Seeder;

class UserUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We get the user units from the remote database
        $userUnits = DB::connection("mysql_remote")
            ->table("user_units")
            ->get();

        // We seed the database with new user units
        foreach ($userUnits as $userUnit) {
            UserUnit::updateOrCreate(["id" => $userUnit->id], [
                "id" => $userUnit->id,
                "user_id" => $userUnit->user_id,
                "unit_id" => $userUnit->unit_id,
                "created_at" => $userUnit->created_at,
                "updated_at" => $userUnit->updated_at
            ]);
        }
    }
}
