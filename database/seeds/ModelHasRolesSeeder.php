<?php

use Illuminate\Database\Seeder;

class ModelHasRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // We get the data from the remote database
        $modelHasRoles = DB::connection("mysql_remote")
            ->table("model_has_roles")
            ->get();

        // We create an auxiliary array that will be inserted into the database
        $insertionArray = [];

        // We loop through all role-permissions relations
        foreach ($modelHasRoles as $modelHasRole) {

            // We update the model namespace as an important part
            $modelNamespace = Str::replaceArray("App\\", ["App\\Models\\"], $modelHasRole->model_type);

            // We add data to the insertion array
            $insertionArray[] = [
                "role_id" => $modelHasRole->role_id,
                "model_type" => $modelNamespace,
                "model_id" => $modelHasRole->model_id
            ];

        }

        // We seed the database with new data
        DB::connection("mysql")
            ->table("model_has_roles")
            ->insertOrIgnore($insertionArray);
    }
}
