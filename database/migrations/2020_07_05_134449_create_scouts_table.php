<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("scouts", function (Blueprint $table) {
            $table->id();
            $table->string("first_name")->nullable(false);
            $table->string("last_name")->nullable(false);
            $table->string("registration_number", 11)->unique();
            $table->unsignedBigInteger("unit_id")->nullable();
            $table->unsignedBigInteger("district_id")->nullable();
            $table->text("remarks")->nullable();
            $table->date("joined_at")->nullable();
            $table->date("quit_at")->nullable();
            $table->boolean("archived")->default(false);
            $table->timestamps();

            $table->foreign("unit_id")
                ->references("id")->on("units")
                ->onUpdate("cascade")
                ->onDelete("set null");

            $table->foreign("district_id")
                ->references("id")->on("districts")
                ->onUpdate("cascade")
                ->onDelete("set null");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("scouts", function (Blueprint $table) {
            $table->dropForeign(["unit_id", "district_id"]);
        });
        Schema::dropIfExists("scouts");
    }
}
