<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("user_units", function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("user_id");
            $table->unsignedBigInteger("unit_id");
            $table->timestamps();

            $table->foreign("user_id")
                ->references("id")->on("users")
                ->onUpdate("cascade")
                ->onDelete("cascade");

            $table->foreign("unit_id")
                ->references("id")->on("units")
                ->onUpdate("cascade")
                ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("user_units", function (Blueprint $table) {
            $table->dropForeign(["user_id", "unit_id"]);
        });
        Schema::dropIfExists("user_units");
    }
}
