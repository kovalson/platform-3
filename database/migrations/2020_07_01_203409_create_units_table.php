<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("units", function (Blueprint $table) {
            $table->id();
            $table->string("name")->nullable(false);
            $table->unsignedBigInteger("district_id")->nullable();
            $table->timestamps();

            $table->foreign("district_id")
                ->references("id")->on("districts")
                ->onUpdate("cascade")
                ->onDelete("set null");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("units", function (Blueprint $table) {
            $table->dropForeign("district_id");
        });
        Schema::dropIfExists("units");
    }
}
