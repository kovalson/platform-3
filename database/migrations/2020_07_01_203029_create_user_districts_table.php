<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("user_districts", function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("user_id");
            $table->unsignedBigInteger("district_id");
            $table->timestamps();

            $table->foreign("user_id")
                ->references("id")->on("users")
                ->onUpdate("cascade")
                ->onDelete("cascade");

            $table->foreign("district_id")
                ->references("id")->on("districts")
                ->onUpdate("cascade")
                ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("user_district", function (Blueprint $table) {
            $table->dropForeign(["user_id", "district_id"]);
        });
        Schema::dropIfExists("user_districts");
    }
}
