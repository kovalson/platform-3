<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContributionsAmountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("contributions_amounts", function (Blueprint $table) {
            $table->year("year")->nullable(false)->primary();
            $table->integer("quarter1")->default(0);
            $table->integer("quarter2")->default(0);
            $table->integer("quarter3")->default(0);
            $table->integer("quarter4")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("contributions_amounts");
    }
}
