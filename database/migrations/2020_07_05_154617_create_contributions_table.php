<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("contributions", function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("scout_id");
            $table->year("year")->nullable(false);
            $table->boolean("quarter1")->nullable(false);
            $table->boolean("quarter2")->nullable(false);
            $table->boolean("quarter3")->nullable(false);
            $table->boolean("quarter4")->nullable(false);
            $table->timestamps();

            $table->foreign("scout_id")
                ->references("id")->on("scouts")
                ->onUpdate("cascade")
                ->onDelete("cascade");

            $table->unique(["scout_id", "year"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("contributions", function (Blueprint $table) {
            $table->dropUnique(["scout_id", "year"]);
            $table->dropForeign("scout_id");
        });
        Schema::dropIfExists("contributions");
    }
}
