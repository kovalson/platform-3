<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolePermissionScopesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("role_permission_scopes", function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("role_id");
            $table->unsignedBigInteger("permission_id");
            $table->string("scope");
            $table->text("values");
            $table->timestamps();

            $table->foreign("role_id")
                ->references("id")->on("roles")
                ->onUpdate("cascade")
                ->onDelete("cascade");

            $table->foreign("permission_id")
                ->references("id")->on("permissions")
                ->onUpdate("cascade")
                ->onDelete("cascade");

            $table->unique([
                "role_id",
                "permission_id",
                "scope"
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("role_permission_scopes", function (Blueprint $table) {
            $table->dropUnique(["role_id", "permission_id", "scope"]);
            $table->dropForeign(["role_id", "permission_id"]);
        });
        Schema::dropIfExists("role_permission_scopes");
    }
}
