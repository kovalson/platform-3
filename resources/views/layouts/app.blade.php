<!doctype html>
<html lang="{{ str_replace("_", "-", app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="title" content="{{ config("app.name") }}">
    <meta name="description" content="{{ __("Sprawdź stan swojej składki członkowskiej ZHP.") }}">
    <meta name="robots" content="index, follow">
    <meta name="author" content="Krzysztof Tatarynowicz, ktatarynowicz@hotmail.com">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset("img/favicon/ms-icon-144x144.png") }}">
    <meta name="theme-color" content="#ffffff">

    <title>{{ config("app.name", __("Platforma Składkowa")) }} - {{ __("Chorągiew Ziemi Lubuskiej ZHP") }}</title>

    {{-- We load compiled Javascript files. --}}
    <script src="{{ route("language") }}"></script>
    <script src="{{ asset("js/app.js") }}" defer></script>

    {{-- We load favicons. --}}
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset("img/favicon/apple-icon-57x57.png") }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset("img/favicon/apple-icon-60x60.png") }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset("img/favicon/apple-icon-72x72.png") }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset("img/favicon/apple-icon-76x76.png") }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset("img/favicon/apple-icon-114x114.png") }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset("img/favicon/apple-icon-120x120.png") }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset("img/favicon/apple-icon-144x144.png") }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset("img/favicon/apple-icon-152x152.png") }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset("img/favicon/apple-icon-180x180.png") }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset("img/favicon/android-icon-192x192.png") }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset("img/favicon/favicon-32x32.png") }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset("img/favicon/favicon-96x96.png") }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset("img/favicon/favicon-16x16.png") }}">
    <link rel="manifest" href="{{ asset("img/favicon/manifest.json") }}">

    {{-- We load the stylesheet. --}}
    <link href="{{ asset("css/app.css") }}" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="app">
        <noscript>
            {{ __("Przykro nam. Platforma Składkowa Chorągwi Ziemi Lubuskiej ZHP nie działa bez włączonej obsługi JavaScript. Włącz obsługę JavaScript w swojej przeglądarce, aby korzystać z Platformy.") }}
        </noscript>
    </div>
</body>
</html>
