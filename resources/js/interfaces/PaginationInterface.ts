export default interface PaginationInterface {

    /**
     * Total count of the resource.
     *
     * @type number
     */
    total: number;

    /**
     * The number of results per page.
     *
     * @type number
     */
    per_page: number;

    /**
     * The current page.
     *
     * @type number
     */
    current_page: number;

    /**
     * The number of the last page.
     *
     * @type number
     */
    last_page: number;

    /**
     * The absolute URL of the first page.
     *
     * @type string|null
     */
    first_page_url: string | null;

    /**
     * The absolute URL of the last page.
     *
     * @type string|null
     */
    last_page_url: string | null;

    /**
     * The absolute URL to the next page.
     *
     * @type string|null
     */
    next_page_url: string | null;

    /**
     * The absolute URL to the previous page.
     *
     * @type string|null
     */
    prev_page_url: string | null;

    /**
     * The path of the request.
     *
     * @type string
     */
    path: string;

    /**
     * The starting offset of the pagination.
     *
     * @type number
     */
    from: number;

    /**
     * The ending offset of the pagination.
     *
     * @type number
     */
    to: number;

}
