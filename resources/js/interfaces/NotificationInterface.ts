export default interface NotificationInterface {
    title?: string;
    message: string;
}
