export interface ContributionsProfileQuartersInterface {
    quarter1: number;
    quarter2: number;
    quarter3: number;
    quarter4: number;
}

export interface ContributionsProfileStatusInterface {
    [year: number]: ContributionsProfileQuartersInterface;
}

export interface ContributionsArrearsInterface {
    total: number;
    array: ContributionsProfileStatusInterface;
    quartersCount: number;
}

export default interface ContributionsProfileInterface {
    scout_id: number;
    years: number[];
    yearsCount: number;
    status: ContributionsProfileStatusInterface;
    arrears: ContributionsArrearsInterface;
}
