import Vue from "vue";
import { NavigationGuard } from "vue-router";

export default interface RouteInterface {

    /**
     * The path of the route in VueRouter.
     *
     * @type string
     */
    path: string;

    /**
     * The name of the route in VueRouter.
     *
     * @type string
     */
    name?: string;

    /**
     * The vue component to render.
     *
     * @type Vue
     */
    component?: typeof Vue;

    /**
     * The navigation guard for the route.
     *
     * @type NavigationGuard
     */
    beforeEnter?: NavigationGuard;

    /**
     * Sets the route name.
     *
     * @param routeName
     * @return RouteInterface
     */
    named(routeName: string): RouteInterface;

    /**
     * Sets the Vue component to be render upon
     * visiting the route.
     *
     * @param {Vue} component
     * @return RouteInterface
     */
    renders(component: typeof Vue): RouteInterface;

    /**
     * Sets the navigation guard that runs before
     * entering the route.
     *
     * @param {NavigationGuard} navigationGuard
     * @return RouteInterface
     */
    guard(navigationGuard: NavigationGuard): RouteInterface;

    /**
     * Sets the navigation guard that runs before
     * entering the route and required the user
     * to have given permission.
     *
     * @param {string} permission
     * @return RouteInterface
     */
    permission(permission: string): RouteInterface;

}
