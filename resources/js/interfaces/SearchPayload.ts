/**
 * SearchPayload interface.
 *
 * Contains the search query typed by the user in the search input.
 * This interface is used when hitting the "Search" button anywhere
 * in the application while emitting an event by the VPageMenuSearch
 * component.
 */
export default interface SearchPayload {
    query: string;
}
