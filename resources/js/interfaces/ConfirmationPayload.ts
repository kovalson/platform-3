import { CancellationCallback, ConfirmationCallback } from "../mixins/ConfirmationMixin";

export default interface ConfirmationPayload {
    question: string;
    title?: string;
    onConfirm?: ConfirmationCallback;
    onCancel?: CancellationCallback;
}
