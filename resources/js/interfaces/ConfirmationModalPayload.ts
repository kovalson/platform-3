import { CancellationCallback, ConfirmationCallback } from "../mixins/ConfirmationMixin";;
import ConfirmationPayload from "./ConfirmationPayload";

export default interface ConfirmationModalPayload extends ConfirmationPayload {
    onConfirm: ConfirmationCallback;
    onCancel: CancellationCallback;
}
