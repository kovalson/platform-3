/**
 * Ambient declaration for VueFormulate module that has
 * no typings provided. This file also provides no
 * typings but just informs TypeScript that there exists
 * such module and it is perfectly fine to use it.
 */
declare module "@braid/vue-formulate";
