import axios from "axios";
import jQuery from "jquery";
import Lodash from "lodash";
import Popper from "popper.js";
import { VueRouter } from "vue-router/types/router";
import permissionsScopes from "../../json/role_permission_scopes.json";
import permissionsDescriptions from "../utils/permissions-descriptions";
import scopesDescriptions from "../utils/scopes-descriptions";

declare module "vue/types/vue" {
    interface Vue {
        $eventBus: Vue;
        $lang(text?: string, args?: any): string;
        $lodash: typeof Lodash;
        __(text?: string, args?: any): string;
        $router: VueRouter;
        allPermissionsDescriptions: typeof permissionsDescriptions;
        permissionsScopes: typeof permissionsScopes;
        scopesDescriptions: typeof scopesDescriptions;
    }
}

declare global {
    interface Window {
        i18n: object;
        Popper: typeof Popper;
        _: typeof Lodash;
        $: typeof jQuery;
        jQuery: typeof jQuery;
        axios: typeof axios;
        $lang(text?: string, args?: any): string;
        __(text?: string, args?: any): string;
    }
}

export {};
