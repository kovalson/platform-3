import Vue from "vue";
import Component from "vue-class-component";
import {
    EVENT_NOTIFICATION_ERROR,
    EVENT_NOTIFICATION_INFO,
    EVENT_NOTIFICATION_SERVER_ERROR,
    EVENT_NOTIFICATION_SUCCESS,
    EVENT_NOTIFICATION_WARNING,
    EventBus
} from "../event-bus";

@Component
export default class NotificationMixin extends Vue {

    /**
     * Fires an event to EventBus to show a toast.
     *
     * @param type
     * @param message
     * @param title
     */
    private static createNotification(type: string, message: string, title?: string): void {
        EventBus.$emit(type, {
            title,
            message
        });
    }

    /**
     * Fires an event to EventBus to show an info toast.
     *
     * @param {string} message
     * @param {string} title
     */
    public notifyInfo(message: string, title?: string): void {
        NotificationMixin.createNotification(EVENT_NOTIFICATION_INFO, message, title);
    }

    /**
     * Fires an event to EventBus to show a success toast.
     *
     * @param {string} message
     * @param {string} title
     */
    public notifySuccess(message: string, title?: string): void {
        NotificationMixin.createNotification(EVENT_NOTIFICATION_SUCCESS, message, title);
    }

    /**
     * Fires an event to EventBus to show a warning toast.
     *
     * @param {string} message
     * @param {string} title
     */
    public notifyWarning(message: string, title?: string): void {
        NotificationMixin.createNotification(EVENT_NOTIFICATION_WARNING, message, title);
    }

    /**
     * Fires an event to EventBus to show an error toast.
     *
     * @param {string} message
     * @param {string} title
     */
    public notifyError(message: string, title?: string): void {
        NotificationMixin.createNotification(EVENT_NOTIFICATION_ERROR, message, title);
    }

    /**
     * Fires an event to EventBus to show a server error toast.
     *
     * @param {string} message
     * @param {string} title
     */
    public notifyServerError(message: string, title?: string): void {
        NotificationMixin.createNotification(EVENT_NOTIFICATION_SERVER_ERROR, message, title);
    }

}
