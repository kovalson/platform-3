const mix = require("laravel-mix");

mix.options({
    vue: {
        compilerOptions: {
            // We want to clear all unnecessary whitespaces, especially
            // those hiding between inline HTML elements causing whitespaces
            // to occur while selecting text on the page
            whitespace: "condense",
            preserveWhitespace: false
        }
    }
});

// We compile resources and create source maps
mix.ts("resources/js/app.ts", "public/js")
    .sass("resources/sass/app.scss", "public/css")
    .sourceMaps();

// Whenever the application is built in production mode
// we want all generated files to contain a "version note"
// inside the filename in order not to be cached by browsers
if (mix.inProduction()) {
    mix.version();
}
